package cuenen.raymond.messaging;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Base class for data messages. This implementation holds the message metadata.
 */
public abstract class AbstractDataMessage implements DataMessage {

    private final Map<String, HeaderProperty> headers = new HashMap<>();
    private boolean readOnly;

    @Override
    public boolean propertyExists(String name) {
        return headers.containsKey(name);
    }

    @Override
    public Enumeration<String> getPropertyNames() {
        return Collections.enumeration(headers.keySet());
    }

    @Override
    public void clearProperties() {
        headers.clear();
        readOnly = false;
    }

    @Override
    public Boolean getBooleanProperty(String name) throws DataMessageException {
        return getHeaderPropertyValue(name, Boolean.class);
    }

    @Override
    public Number getNumberProperty(String name) throws DataMessageException {
        return getHeaderPropertyValue(name, Number.class);
    }

    @Override
    public String getStringProperty(String name) throws DataMessageException {
        return getHeaderPropertyValue(name, String.class);
    }

    @Override
    public Object getObjectProperty(String name) throws DataMessageException {
        return getHeaderPropertyValue(name, Object.class);
    }

    @Override
    public void setBooleanProperty(String name, Boolean value) throws DataMessageException {
        setObjectProperty(name, value);
    }

    @Override
    public void setNumberProperty(String name, Number value) throws DataMessageException {
        setObjectProperty(name, value);
    }

    @Override
    public void setStringProperty(String name, String value) throws DataMessageException {
        setObjectProperty(name, value);
    }

    @Override
    public void setObjectProperty(String name, Object value) throws DataMessageException {
        if (value == null) {
            setHeaderProperty(HeaderProperty.createNullProperty(name));
        } else if (value instanceof Boolean) {
            setHeaderProperty(HeaderProperty.createBooleanProperty(name, (Boolean) value));
        } else if (value instanceof Number) {
            setHeaderProperty(HeaderProperty.createNumberProperty(name, (Number) value));
        } else if (value instanceof String) {
            setHeaderProperty(HeaderProperty.createStringProperty(name, (String) value));
        } else {
            throw new MessageFormatException("Unsupported property type: " + value.getClass());
        }
    }

    @Override
    public final void onMarshall() {
        this.readOnly = true;
    }

    @SuppressWarnings("unchecked")
    private <P> P getHeaderPropertyValue(String name, Class<P> to) throws DataMessageException {
        final HeaderProperty property = headers.get(name);
        if (property == null) {
            throw new MessagePropertyNotFoundException("Property " + name + " not found");
        }
        final Object value = property.getValue();
        if (value == null) {
            return null;
        } else if (to.isInstance(value)) {
            return to.cast(value);
        } else {
            final Class<?> from = value.getClass();
            final String rawValue = String.valueOf(value);
            if (String.class.equals(from) && Boolean.class.equals(to)) {
                return (P) Boolean.valueOf(rawValue);
            } else if (String.class.equals(from) && Number.class.equals(to)) {
                try {
                    return (P) NumberConverter.valueOf(rawValue);
                } catch (NumberFormatException ex) {
                    throw new MessageFormatException(ex.getMessage());
                }
            } else if (String.class.equals(to)) {
                return (P) rawValue;
            }
        }
        throw new MessageFormatException("Property " + name + " is a " + value.getClass().getName() + " and cannot be read as " + to.getName());
    }

    private void setHeaderProperty(HeaderProperty property) throws DataMessageException {
        if (readOnly) {
            throw new MessageNotWriteableException("Header properties are read-only");
        }
        headers.put(property.getName(), property);
    }

    @Override
    public int hashCode() {
        return DataMessageUtil.hashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DataMessage) {
            return DataMessageUtil.equals(this, (DataMessage) obj);
        }
        return false;
    }

    @Override
    public String toString() {
        return getMessageType() + headers;
    }
}
