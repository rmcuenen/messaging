package cuenen.raymond.messaging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for converting between UTC, TAI and GPS. UTC is given as the
 * number of milliseconds since January 1, 1970, 00:00:00 GMT and TAI is given
 * as the number of milliseconds since the configured epoch (given in UTC). GPS
 * has a constant offset to TAI of 19 seconds.
 */
public final class AtomicTime {

    private static final Logger Logger = LoggerFactory.getLogger(AtomicTime.class);
    private static final SortedSet<Long> LeapSeconds = new TreeSet<>();
    private static final AtomicLong Epoch = new AtomicLong(0);
    private static final AtomicLong Offset = new AtomicLong(0);
    private static final long Second = 1000L;
    private static final long GPSTimeOffset = 19 * Second;

    static {
        try {
            final Properties props = new Properties();
            props.load(AtomicTime.class.getResourceAsStream("/tai.dat"));
            final DateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            parser.setTimeZone(TimeZone.getTimeZone("UTC"));
            Epoch.set(parser.parse(props.getProperty("epoch")).getTime());
            Offset.set(Long.valueOf(props.getProperty("offset", "0")) * Second);
            String date = props.getProperty("leap.second.1");
            int count = 2;
            while (date != null) {
                LeapSeconds.add(parser.parse(date).getTime());
                date = props.getProperty("leap.second." + count++);
            }
        } catch (Exception ex) {
            Logger.warn("Unable to initialize TAI offset: {}", ex.toString());
        }
    }

    /**
     * Convert the number of milliseconds given in UTC into the number of
     * milliseconds in TAI.
     *
     * @param utc The number of milliseconds in UTC.
     * @return The number of milliseconds in TAI.
     */
    public static long utcToTAI(long utc) {
        long tai = utc;
        for (long ls : LeapSeconds) {
            if (tai < ls) {
                break;
            }
            tai += Second;
        }
        return tai - Epoch.get() + Offset.get();
    }

    /**
     * Convert the number of milliseconds given in UTC into the number of
     * milliseconds in GPS.
     *
     * @param utc The number of milliseconds in UTC.
     * @return The number of milliseconds in GPS.
     */
    public static long utcToGPS(long utc) {
        return taiToGPS(utcToTAI(utc));
    }

    /**
     * Convert the number of milliseconds given in TAI into the number of
     * milliseconds in UTC.
     *
     * @param tai The number of milliseconds in TAI.
     * @return The number of milliseconds in UTC.
     */
    public static long taiToUTC(long tai) {
        long utc = tai + Epoch.get() - Offset.get();
        for (long ls : LeapSeconds) {
            if (utc < ls) {
                break;
            }
            utc -= Second;
        }
        return utc;
    }

    /**
     * Convert the number of milliseconds given in TAI into the number of
     * milliseconds in GPS.
     *
     * @param tai The number of milliseconds in TAI.
     * @return The number of milliseconds in GPS.
     */
    public static long taiToGPS(long tai) {
        return tai - GPSTimeOffset;
    }

    /**
     * Convert the number of milliseconds given in GPS into the number of
     * milliseconds in UTC.
     *
     * @param gps The number of milliseconds in GPS.
     * @return The number of milliseconds in UTC.
     */
    public static long gpsToUTC(long gps) {
        return taiToUTC(gpsToTAI(gps));
    }

    /**
     * Convert the number of milliseconds given in GPS into the number of
     * milliseconds in TAI.
     *
     * @param gps The number of milliseconds in GPS.
     * @return The number of milliseconds in TAI.
     */
    public static long gpsToTAI(long gps) {
        return gps + GPSTimeOffset;
    }

    /**
     * Private constructor.
     */
    private AtomicTime() {
        // Utility class
    }
}
