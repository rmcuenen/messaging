package cuenen.raymond.messaging;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * The properties needed for the client to connect and communicate with a
 * broker. The basic connection properties are:
 * <ul>
 * <li>{@link #BROKER_URL The broker URL}</li>
 * <li>{@link #USERNAME The user name}</li>
 * <li>{@link #PASSWORD The password}</li>
 * </ul>
 * For other properties refer to the client implementations.
 */
public class BrokerProperties {

    /**
     * The key for the property of the connection URL used to connect to the
     * message broker.
     */
    public static final String BROKER_URL = "brokerUrl";
    /**
     * The key for the property of the user name to use when connecting to the
     * message broker.
     */
    public static final String USERNAME = "username";
    /**
     * The key for the property of the password to use when connecting to the
     * message broker.
     */
    public static final String PASSWORD = "password";

    private final Map<String, Object> properties = new ConcurrentHashMap<>();

    /**
     * Set the default broker properties for keys not set to any non-null value.
     *
     * @param defaults The default properties.
     */
    public void setDefaults(Map<String, String> defaults) {
        defaults.entrySet().stream()
                .filter(e -> properties.get(e.getKey()) == null)
                .forEach(dev -> properties.put(dev.getKey(), dev.getValue()));
    }

    /**
     * Returns the value of the {@code Boolean} property with the specified
     * name.
     *
     * @param name The name of the {@code Boolean} property.
     * @return The {@code Boolean} property value for the specified name, or
     * {@code null} when not found or not a boolean.
     */
    public Boolean getBooleanProperty(String name) {
        return getPropertyValue(name, Boolean.class);
    }

    /**
     * Returns the value of the {@code Number} property with the specified name.
     *
     * @param name The name of the {@code Number} property.
     * @return The {@code Number} property value for the specified name, or
     * {@code null} when not found or not a number.
     */
    public Number getNumberProperty(String name) {
        return getPropertyValue(name, Number.class);
    }

    /**
     * Returns the value of the {@code String} property with the specified name.
     *
     * @param name The name of the {@code String} property.
     * @return The {@code String} property value for the specified name, or
     * {@code null} when not found.
     */
    public String getStringProperty(String name) {
        return getPropertyValue(name, String.class);
    }

    /**
     * Returns the value of the Java object property with the specified name.
     *
     * @param name The name of the Java object property.
     * @return The Java object property value with the specified name, or
     * {@code null} when not found.
     */
    public Object getObjectProperty(String name) {
        return getPropertyValue(name, Object.class);
    }

    /**
     * Sets a {@code Boolean} property value with the specified name.
     *
     * @param name The name of the {@code Boolean} property.
     * @param value The {@code Boolean} property value to set.
     */
    public void setBooleanProperty(String name, Boolean value) {
        setObjectProperty(name, value);
    }

    /**
     * Sets a {@code Number} property value with the specified name.
     *
     * @param name The name of the {@code Number} property.
     * @param value The {@code Number} property value to set.
     */
    public void setNumberProperty(String name, Number value) {
        setObjectProperty(name, value);
    }

    /**
     * Sets a {@code String} property value with the specified name.
     *
     * @param name The name of the {@code String} property.
     * @param value The {@code String} property value to set.
     */
    public void setStringProperty(String name, String value) {
        setObjectProperty(name, value);
    }

    /**
     * Sets a Java object property value with the specified name.
     *
     * @param name The name of the Java object property.
     * @param value The Java object property value to set.
     */
    public void setObjectProperty(String name, Object value) {
        properties.put(name, value);
    }

    @SuppressWarnings("unchecked")
    private <P> P getPropertyValue(String name, Class<P> to) {
        final Object value = properties.get(name);
        if (value == null) {
            return null;
        } else if (to.isInstance(value)) {
            return to.cast(value);
        } else {
            final Class<?> from = value.getClass();
            final String rawValue = String.valueOf(value);
            if (String.class.equals(from) && Boolean.class.equals(to)) {
                return (P) Boolean.valueOf(rawValue);
            } else if (String.class.equals(from) && Number.class.equals(to)) {
                try {
                    return (P) NumberConverter.valueOf(rawValue);
                } catch (NumberFormatException ex) {
                    return null;
                }
            } else if (String.class.equals(to)) {
                return (P) rawValue;
            }
        }
        return null;
    }

    @Override
    public int hashCode() {
        return properties.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BrokerProperties) {
            return ((BrokerProperties) obj).properties.equals(this.properties);
        }
        return false;
    }

    @Override
    public String toString() {
        return "BrokerProperties" + properties;
    }

}
