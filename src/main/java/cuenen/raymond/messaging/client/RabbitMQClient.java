package cuenen.raymond.messaging.client;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AMQP.Exchange.Declare;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.LongString;
import com.rabbitmq.client.QueueingConsumer;
import cuenen.raymond.messaging.BrokerProperties;
import static cuenen.raymond.messaging.BrokerProperties.*;
import cuenen.raymond.messaging.Client;
import static cuenen.raymond.messaging.Client.MessageTypeHeader;
import cuenen.raymond.messaging.DataMessage;
import static cuenen.raymond.messaging.MessageBroker.getMessage;
import cuenen.raymond.messaging.MessageConverter;
import cuenen.raymond.messaging.Request;
import cuenen.raymond.messaging.Subscriber;
import cuenen.raymond.messaging.Timeout;
import cuenen.raymond.messaging.Version;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The RabbitMQ client is used to connect to the RabbitMQ message broker.
 */
public class RabbitMQClient implements Client {

    /**
     * Key for the property of the exchange type.
     */
    public static final String EXCHANGE_TYPE = "exchangeType";
    /**
     * Key for the property of the durable setting.
     */
    public static final String DURABLE = "durable";
    /**
     * Key for the property of the autoDelete setting.
     */
    public static final String AUTO_DELETE = "autoDelete";

    private static final Logger LOG = LoggerFactory.getLogger(RabbitMQClient.class);
    private static final Map<String, String> DEFAULTS = new HashMap<>();

    static {
        LOG.info(Version.Current.toString());
        DEFAULTS.put(BROKER_URL, "amqp://" + ConnectionFactory.DEFAULT_HOST);
        DEFAULTS.put(USERNAME, ConnectionFactory.DEFAULT_USER);
        DEFAULTS.put(PASSWORD, ConnectionFactory.DEFAULT_PASS);
        DEFAULTS.put(EXCHANGE_TYPE, "headers");
        DEFAULTS.put(DURABLE, "true");
        DEFAULTS.put(AUTO_DELETE, "false");
    }

    /**
     * Creates a new {@link RabbitMQClient}.
     *
     * @param messageConverter The message converter.
     * @param properties The broker properties.
     * @return The client implementation for RabbitMQ.
     */
    public static Client create(MessageConverter messageConverter, BrokerProperties properties) {
        properties.setDefaults(DEFAULTS);
        final ConnectionFactory connectionFactory = new ConnectionFactory();
        final String brokerURL = properties.getStringProperty(BROKER_URL);
        try {
            connectionFactory.setUri(brokerURL);
        } catch (URISyntaxException | NoSuchAlgorithmException | KeyManagementException e) {
            throw new IllegalArgumentException("Invalid broker URI: " + brokerURL, e);
        }
        connectionFactory.setUsername(properties.getStringProperty(USERNAME));
        connectionFactory.setPassword(properties.getStringProperty(PASSWORD));
        return new RabbitMQClient(messageConverter, connectionFactory,
                new Declare.Builder()
                .exchange("")
                .type(properties.getStringProperty(RabbitMQClient.EXCHANGE_TYPE))
                .durable(properties.getBooleanProperty(RabbitMQClient.DURABLE))
                .autoDelete(properties.getBooleanProperty(RabbitMQClient.AUTO_DELETE))
                .build());
    }

    private final Map<String, Subscription> subscribers = new ConcurrentHashMap<>();
    private final ConnectionFactory connectionFactory;
    private final MessageConverter messageConverter;
    private final Declare exchangeDeclare;
    private com.rabbitmq.client.Consumer consumer;
    private Connection connection;
    private Channel channel;

    /**
     * Creates a new {@link RabbitMQClient}.
     *
     * @param messageConverter The {@link MessageConverter} for encoding and
     * decoding messages.
     * @param connectionFactory The connection factory for opening a
     * {@link Connection} to the message broker.
     * @param exchangeDeclare The exchange type, durable and autoDelete settings
     * for exchanges.
     */
    public RabbitMQClient(MessageConverter messageConverter,
            ConnectionFactory connectionFactory, Declare exchangeDeclare) {
        this.connectionFactory = connectionFactory;
        this.messageConverter = messageConverter;
        this.exchangeDeclare = exchangeDeclare;
    }

    @Override
    public void connect() throws Exception {
        LOG.trace("Connecting to message broker");
        if (connection == null) {
            connection = connectionFactory.newConnection();
        }
        if (channel == null) {
            channel = connection.createChannel();
            consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope,
                        AMQP.BasicProperties properties, byte[] body) throws IOException {
                    final Subscription subscription = subscribers.get(envelope.getExchange());
                    if (subscription != null) {
                        final String messageType = String.valueOf(properties.getHeaders().get(MessageTypeHeader));
                        try {
                            final DataMessage dataMessage = messageConverter.decode(
                                    String.valueOf(messageType),
                                    body);
                            for (Entry<String, Object> entry : properties.getHeaders().entrySet()) {
                                // Convert LongStrings to String.
                                if (entry.getValue() instanceof LongString) {
                                    dataMessage.setStringProperty(entry.getKey(), entry.getValue().toString());
                                } else {
                                    dataMessage.setObjectProperty(entry.getKey(), entry.getValue());
                                }
                            }
                            dataMessage.onMarshall();
                            subscription.publish(consumerTag, dataMessage);
                        } catch (Exception ex) {
                            LOG.warn("Error receiving published message '{}' - {}",
                                    messageType, getMessage(ex));
                        }
                    }
                }
            };
        }
    }

    @Override
    public void subscribe(Subscriber subscriber) {
        if (channel == null) {
            notConnected();
        }
        unsubscribe(subscriber);
        final String topicName = subscriber.getTopicName();
        final Consumer<? extends DataMessage> callback = subscriber.getCallback();
        Subscription subscription = subscribers.get(topicName);
        try {
            if (subscription == null) {
                LOG.trace("Creating subscription for '{}'", topicName);
                channel.exchangeDeclare(topicName,
                        exchangeDeclare.getType(),
                        exchangeDeclare.getDurable(),
                        exchangeDeclare.getAutoDelete(),
                        null);
                subscription = new Subscription();
                subscribers.put(topicName, subscription);
            }
            LOG.debug("Subscribing '{}'", subscriber);
            String queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, topicName, "",
                    subscriber.getMessageSelector().toBindArguments());
            channel.basicConsume(queueName, true, subscriber.getSubscriberId(), consumer);
            subscription.put(subscriber.getSubscriberId(), callback);
        } catch (IOException ex) {
            LOG.error("Error creating subscription for '{}' - {}",
                    topicName, getMessage(ex));
        }
    }

    @Override
    public void unsubscribe(Subscriber subscriber) {
        final String topicName = subscriber.getTopicName();
        final Subscription subscription = subscribers.get(topicName);
        if (subscription != null) {
            LOG.debug("Unsubscribing '{}'", subscriber);
            final Consumer<? extends DataMessage> callback = subscription.remove(subscriber.getSubscriberId());
            if (callback != null) {
                try {
                    channel.basicCancel(subscriber.getSubscriberId());
                    if (subscription.isEmpty()) {
                        LOG.trace("Removing subscription for '{}'", topicName);
                        subscribers.remove(topicName);
                    }
                } catch (IOException ex) {
                    LOG.error("Error removing subscription for '{}' - {}",
                            topicName, getMessage(ex));
                }
            }
        }
    }

    @Override
    public void publish(String topicName, DataMessage data) throws Exception {
        if (channel == null) {
            notConnected();
        }
        channel.exchangeDeclare(topicName,
                exchangeDeclare.getType(),
                exchangeDeclare.getDurable(),
                exchangeDeclare.getAutoDelete(),
                null);
        final Map<String, Object> headers = new HashMap<>();
        setMessageProperties(headers, data);
        final AMQP.BasicProperties props = new AMQP.BasicProperties.Builder()
                .userId(connectionFactory.getUsername())
                .headers(headers)
                .build();
        LOG.debug("Publishing data to '{}'", topicName);
        channel.basicPublish(topicName, "", props, messageConverter.encode(data));
    }

    @Override
    public CompletableFuture<? extends DataMessage> send(Request request) {
        if (channel == null) {
            throw new IllegalStateException("Not connected");
        }
        final CompletableFuture<DataMessage> response = new CompletableFuture<>();
        CompletableFuture.runAsync(() -> {
            try {
                final String queueName = request.getQueueName();
                final DataMessage message = request.getMessage();
                final long timeout = request.getReplyTimeout().toMillis();
                final String replyQueueName = channel.queueDeclare().getQueue();
                final QueueingConsumer replyConsumer = new QueueingConsumer(channel);
                channel.basicConsume(replyQueueName, true, replyQueueName, replyConsumer);
                final Map<String, Object> headers = new HashMap<>();
                setMessageProperties(headers, message);
                final AMQP.BasicProperties props = new AMQP.BasicProperties.Builder()
                        .correlationId(Long.toString(request.getCorrelationId()))
                        .replyTo(replyQueueName)
                        .userId(connectionFactory.getUsername())
                        .headers(headers)
                        .build();
                LOG.debug("Sending request to '{}'", queueName);
                channel.basicPublish("", queueName, props, messageConverter.encode(message));
                try {
                    long toWait = timeout;
                    do {
                        final long timestamp = System.nanoTime();
                        final QueueingConsumer.Delivery delivery = replyConsumer.nextDelivery(toWait);
                        if (delivery == null) {
                            throw new TimeoutException();
                        } else if (delivery.getProperties().getCorrelationId().equals(props.getCorrelationId())) {
                            LOG.debug("Received response for '{}'", props.getCorrelationId());
                            final Object messageType = delivery.getProperties().getHeaders().get(MessageTypeHeader);
                            final DataMessage dataMessage = messageConverter.decode(
                                    String.valueOf(messageType),
                                    delivery.getBody());
                            for (Entry<String, Object> entry : delivery.getProperties().getHeaders().entrySet()) {
                                dataMessage.setObjectProperty(entry.getKey(), entry.getValue());
                            }
                            dataMessage.onMarshall();
                            response.complete(dataMessage);
                            break;
                        }
                        final Timeout time = Timeout.fromNanos(System.nanoTime() - timestamp);
                        toWait -= time.toMillis();
                    } while (true);
                } catch (Exception ex) {
                    LOG.error("Error handling response - {}", getMessage(ex));
                    response.completeExceptionally(ex);
                } finally {
                    channel.basicCancel(replyQueueName);
                }
            } catch (Exception ex) {
                LOG.error("Error executing request - {}", getMessage(ex));
                response.completeExceptionally(ex);
            }
        });
        return response;
    }

    @Override
    public void close() throws Exception {
        LOG.trace("Disconnecting from message broker");
        for (Subscription subscription : subscribers.values()) {
            subscription.clear(channel);
        }
        subscribers.clear();
        if (channel != null) {
            channel.close();
            channel = null;
            consumer = null;
        }
        if (connection != null) {
            connection.close();
            connection = null;
        }
    }

    private void setMessageProperties(Map<String, Object> headers, DataMessage message) throws Exception {
        for (Enumeration<String> names = message.getPropertyNames(); names.hasMoreElements();) {
            final String name = names.nextElement();
            headers.put(name, message.getObjectProperty(name));
        }
        headers.put(MessageTypeHeader, message.getMessageType());
        message.onMarshall();
    }

    private void notConnected() {
        throw new IllegalStateException("Not connected");
    }

    private static class Subscription {

        private final Map<String, Consumer<DataMessage>> subscribers = new HashMap<>();

        public void put(String subscriber, Consumer<? extends DataMessage> callback) {
            subscribers.put(subscriber, (Consumer<DataMessage>) callback);
        }

        public Consumer<? extends DataMessage> remove(String subscriber) {
            return subscribers.remove(subscriber);
        }

        public boolean isEmpty() {
            return subscribers.isEmpty();
        }

        public void publish(String subscriberId, DataMessage message) {
            LOG.debug("Publishing received message");
            final Consumer<DataMessage> callback = subscribers.get(subscriberId);
            if (callback != null) {
                try {
                    callback.accept(message);
                } catch (Throwable ex) { //NOSONAR - Explictly want to catch Throwable here
                    LOG.error("Error handling published message - {}", getMessage(ex));
                }
            }
        }

        public void clear(Channel channel) throws IOException {
            for (String subscriber : subscribers.keySet()) {
                channel.basicCancel(subscriber);
            }
            subscribers.clear();
        }
    }
}
