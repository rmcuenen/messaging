package cuenen.raymond.messaging.client;

import cuenen.raymond.messaging.Client;
import static cuenen.raymond.messaging.Client.MessageTypeHeader;
import cuenen.raymond.messaging.DataMessage;
import static cuenen.raymond.messaging.MessageBroker.getMessage;
import cuenen.raymond.messaging.MessageConverter;
import cuenen.raymond.messaging.Request;
import cuenen.raymond.messaging.Subscriber;
import cuenen.raymond.messaging.Timeout;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TemporaryQueue;
import javax.jms.Topic;
import org.slf4j.Logger;

/**
 * The JMS client is used to connect to a Java Message Service message broker.
 */
public abstract class JMSClient implements Client, MessageListener {

    private final Map<String, Subscription> subscribers = new ConcurrentHashMap<>();
    private final ConnectionFactory connectionFactory;
    private final MessageConverter messageConverter;
    private Connection connection;

    /**
     * Creates a new {@link JMSClient}.
     *
     * @param messageConverter The {@link MessageConverter} for encoding and
     * decoding messages.
     * @param connectionFactory The connection factory for opening a
     * {@link Connection} to the message broker.
     */
    public JMSClient(MessageConverter messageConverter, ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
        this.messageConverter = messageConverter;
    }

    /**
     * Retrieve the {@link Logger} associated with this JMS client.
     *
     * @return The logger.
     */
    public abstract Logger log();

    @Override
    public void connect() throws Exception {
        log().trace("Connecting to message broker");
        if (connection == null) {
            connection = connectionFactory.createConnection();
            connection.start();
        }
    }

    @Override
    public void subscribe(Subscriber subscriber) {
        if (connection == null) {
            notConnected();
        }
        unsubscribe(subscriber);
        final String topicName = subscriber.getTopicName();
        final Consumer<? extends DataMessage> callback = subscriber.getCallback();
        Subscription subscription = subscribers.get(topicName);
        try {
            if (subscription == null) {
                log().trace("Creating subscription for '{}'", topicName);
                final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                final Destination destination = session.createTopic(topicName);
                final MessageConsumer consumer = session.createConsumer(destination,
                        subscriber.getMessageSelector().toExpression());
                consumer.setMessageListener(this);
                subscription = new Subscription(session, consumer, log());
                subscribers.put(topicName, subscription);
            }
            log().debug("Subscribing '{}'", subscriber);
            subscription.put(subscriber.getSubscriberId(), callback);
        } catch (JMSException ex) {
            log().error("Error creating subscription for '{}' - {}",
                    topicName, getMessage(ex));
        }
    }

    @Override
    public void unsubscribe(Subscriber subscriber) {
        final String topicName = subscriber.getTopicName();
        final Subscription subscription = subscribers.get(topicName);
        if (subscription != null) {
            log().debug("Unsubscribing '{}'", subscriber);
            final Consumer<? extends DataMessage> callback = subscription.remove(subscriber.getSubscriberId());
            if (callback != null && subscription.isEmpty()) {
                try {
                    log().trace("Removing subscription for '{}'", topicName);
                    subscribers.remove(topicName);
                    subscription.clear();
                } catch (JMSException ex) {
                    log().error("Error removing subscription for '{}' - {}",
                            topicName, getMessage(ex));
                }
            }
        }
    }

    @Override
    public void publish(String topicName, DataMessage data) throws Exception {
        if (connection == null) {
            notConnected();
        }
        try (Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE)) {
            final Destination destination = session.createTopic(topicName);
            try (MessageProducer producer = session.createProducer(destination)) {
                final BytesMessage bytesMessage = session.createBytesMessage();
                setMessageProperties(bytesMessage, data);
                bytesMessage.writeBytes(messageConverter.encode(data));
                log().debug("Publishing data to '{}'", topicName);
                producer.send(bytesMessage);
            }
        }
    }

    @Override
    public CompletableFuture<? extends DataMessage> send(Request request) {
        if (connection == null) {
            notConnected();
        }
        final CompletableFuture<DataMessage> response = new CompletableFuture<>();
        CompletableFuture.runAsync(() -> {
            try {
                final String queueName = request.getQueueName();
                final DataMessage dataMessage = request.getMessage();
                final long timeout = request.getReplyTimeout().toMillis();
                final Session requestSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                final Destination destination = requestSession.createQueue(queueName);
                final MessageProducer producer = requestSession.createProducer(destination);
                producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
                final TemporaryQueue tempDestination = requestSession.createTemporaryQueue();
                final MessageConsumer responseConsumer = requestSession.createConsumer(tempDestination);
                final BytesMessage bytesMessage = requestSession.createBytesMessage();
                setMessageProperties(bytesMessage, dataMessage);
                bytesMessage.writeBytes(messageConverter.encode(dataMessage));
                bytesMessage.setJMSReplyTo(tempDestination);
                bytesMessage.setJMSCorrelationID(Long.toString(request.getCorrelationId()));
                log().debug("Sending request to '{}'", queueName);
                producer.send(bytesMessage);
                try {
                    long toWait = timeout;
                    do {
                        final long timestamp = System.nanoTime();
                        final Message message = responseConsumer.receive(toWait);
                        if (message == null) {
                            throw new TimeoutException();
                        } else if (message.getJMSCorrelationID().equals(bytesMessage.getJMSCorrelationID())
                                && message instanceof BytesMessage) {
                            log().debug("Received response for '{}'", bytesMessage.getJMSCorrelationID());
                            final BytesMessage bytesResponse = (BytesMessage) message;
                            final byte[] body = new byte[(int) bytesResponse.getBodyLength()];
                            bytesResponse.readBytes(body);
                            final DataMessage responseMessage = messageConverter.decode(
                                    message.getStringProperty(MessageTypeHeader),
                                    body);
                            for (Enumeration names = message.getPropertyNames(); names.hasMoreElements();) {
                                String name = String.valueOf(names.nextElement());
                                responseMessage.setObjectProperty(name, message.getObjectProperty(name));
                            }
                            responseMessage.onMarshall();
                            response.complete(responseMessage);
                            break;
                        }
                        final Timeout time = Timeout.fromNanos(System.nanoTime() - timestamp);
                        toWait -= time.toMillis();
                    } while (true);
                } catch (Exception ex) {
                    log().error("Error handling response - {}", getMessage(ex));
                    response.completeExceptionally(ex);
                } finally {
                    responseConsumer.close();
                    tempDestination.delete();
                    producer.close();
                    requestSession.close();
                }
            } catch (Exception ex) {
                log().error("Error executing request - {}", getMessage(ex));
                response.completeExceptionally(ex);
            }
        });
        return response;
    }

    @Override
    public void close() throws Exception {
        log().trace("Disconnecting from message broker");
        for (Subscription subscription : subscribers.values()) {
            subscription.clear();
        }
        subscribers.clear();
        if (connection != null) {
            connection.close();
            connection = null;
        }
    }

    @Override
    public void onMessage(Message message) {
        if (message instanceof BytesMessage) {
            try {
                final Destination destination = message.getJMSDestination();
                if (destination instanceof Topic) {
                    Topic topic = (Topic) destination;
                    final Subscription subscription = subscribers.get(topic.getTopicName());
                    if (subscription != null) {
                        final BytesMessage bytesMessage = (BytesMessage) message;
                        final byte[] body = new byte[(int) bytesMessage.getBodyLength()];
                        bytesMessage.readBytes(body);
                        final DataMessage dataMessage = messageConverter.decode(
                                message.getStringProperty(MessageTypeHeader),
                                body);
                        for (Enumeration names = message.getPropertyNames(); names.hasMoreElements();) {
                            String name = String.valueOf(names.nextElement());
                            dataMessage.setObjectProperty(name, message.getObjectProperty(name));
                        }
                        dataMessage.onMarshall();
                        subscription.publish(dataMessage);
                    }
                }
            } catch (Exception ex) {
                log().warn("Error receiving published message '{}' - {}", message, getMessage(ex));
            }
        }
    }

    private void setMessageProperties(Message message, DataMessage dataMessage) throws Exception {
        for (Enumeration<String> names = dataMessage.getPropertyNames(); names.hasMoreElements();) {
            final String name = names.nextElement();
            message.setObjectProperty(name, dataMessage.getObjectProperty(name));
        }
        message.setStringProperty(MessageTypeHeader, dataMessage.getMessageType());
        dataMessage.onMarshall();
    }

    private void notConnected() {
        throw new IllegalStateException("Not connected");
    }

    private static class Subscription {

        private final Map<String, Consumer<DataMessage>> subscribers = new HashMap<>();
        private final Session session;
        public final MessageConsumer consumer;
        private final Logger logger;

        public Subscription(Session session, MessageConsumer consumer, Logger logger) {
            this.session = session;
            this.consumer = consumer;
            this.logger = logger;
        }

        public void put(String subscriber, Consumer<? extends DataMessage> callback) {
            subscribers.put(subscriber, (Consumer<DataMessage>) callback);
        }

        public Consumer<? extends DataMessage> remove(String subscriber) {
            return subscribers.remove(subscriber);
        }

        public boolean isEmpty() {
            return subscribers.isEmpty();
        }

        public void publish(DataMessage message) {
            logger.debug("Publishing received message");
            subscribers.values().stream().forEach(callback -> {
                try {
                    callback.accept(message);
                } catch (Throwable ex) {
                    logger.error("Error handling published message - {}", getMessage(ex));
                }
            });
        }

        public void clear() throws JMSException {
            subscribers.clear();
            consumer.close();
            session.close();
        }
    }
}
