package cuenen.raymond.messaging.client;

import cuenen.raymond.messaging.BrokerProperties;
import static cuenen.raymond.messaging.BrokerProperties.*;
import cuenen.raymond.messaging.Client;
import cuenen.raymond.messaging.MessageConverter;
import cuenen.raymond.messaging.Version;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import net.timewalker.ffmq3.jndi.FFMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The FFMQ client is used to connect to the FFMQ message broker.
 */
public class FFMQClient extends JMSClient {

    private static final Logger LOG = LoggerFactory.getLogger(FFMQClient.class);

    static {
        LOG.info(Version.Current.toString());
    }

    /**
     * Creates a new {@link FFMQClient}.
     *
     * @param messageConverter The message converter.
     * @param properties The broker properties.
     * @return The client implementation for FFMQ.
     */
    public static Client create(MessageConverter messageConverter, BrokerProperties properties) {
        final FFMQConnectionFactory connectionFactory = new FFMQConnectionFactory();
        connectionFactory.setProviderURL(properties.getStringProperty(BROKER_URL));
        connectionFactory.setSecurityPrincipal(properties.getStringProperty(USERNAME));
        connectionFactory.setSecurityCredentials(properties.getStringProperty(PASSWORD));
        return new FFMQClient(messageConverter, connectionFactory);
    }

    /**
     * Creates a new {@link FFMQClient}.
     *
     * @param messageConverter The {@link MessageConverter} for encoding and
     * decoding messages.
     * @param connectionFactory The connection factory for opening a
     * {@link Connection} to the message broker.
     */
    public FFMQClient(MessageConverter messageConverter, ConnectionFactory connectionFactory) {
        super(messageConverter, connectionFactory);
    }

    @Override
    public Logger log() {
        return LOG;
    }

}
