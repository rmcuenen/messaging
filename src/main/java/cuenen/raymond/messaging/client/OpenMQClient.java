package cuenen.raymond.messaging.client;

import com.sun.messaging.ConnectionConfiguration;
import cuenen.raymond.messaging.BrokerProperties;
import static cuenen.raymond.messaging.BrokerProperties.*;
import cuenen.raymond.messaging.Client;
import cuenen.raymond.messaging.MessageConverter;
import cuenen.raymond.messaging.Version;
import java.util.HashMap;
import java.util.Map;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The OpenMQ client is used to connect to the OpenMQ message broker.
 */
public class OpenMQClient extends JMSClient {

    private static final Logger LOG = LoggerFactory.getLogger(OpenMQClient.class);
    private static final Map<String, String> DEFAULTS = new HashMap<>();

    static {
        LOG.info(Version.Current.toString());
        DEFAULTS.put(USERNAME, "guest");
        DEFAULTS.put(PASSWORD, "guest");
    }

    /**
     * Creates a new {@link OpenMQClient}.
     *
     * @param messageConverter The message converter.
     * @param properties The broker properties.
     * @return The client implementation for OpenMQ.
     */
    public static Client create(MessageConverter messageConverter, BrokerProperties properties) {
        properties.setDefaults(DEFAULTS);
        final com.sun.messaging.ConnectionFactory connectionFactory = new com.sun.messaging.ConnectionFactory();
        final String brokerURL = properties.getStringProperty(BROKER_URL);
        if (brokerURL != null) {
            try {
                connectionFactory.setProperty(ConnectionConfiguration.imqAddressList, brokerURL);
            } catch (JMSException e) {
                throw new IllegalArgumentException("Invalid broker URI: " + brokerURL, e);
            }
        }
        try {
            connectionFactory.setProperty(ConnectionConfiguration.imqSetJMSXUserID, "true");
            connectionFactory.setProperty(ConnectionConfiguration.imqDefaultUsername, properties.getStringProperty(USERNAME));
            connectionFactory.setProperty(ConnectionConfiguration.imqDefaultPassword, properties.getStringProperty(PASSWORD));
        } catch (JMSException e) {
            throw new IllegalArgumentException(e);
        }
        return new OpenMQClient(messageConverter, connectionFactory);
    }

    /**
     * Creates a new {@link OpenMQClient}.
     *
     * @param messageConverter The {@link MessageConverter} for encoding and
     * decoding messages.
     * @param connectionFactory The connection factory for opening a
     * {@link Connection} to the message broker.
     */
    public OpenMQClient(MessageConverter messageConverter, ConnectionFactory connectionFactory) {
        super(messageConverter, connectionFactory);
    }

    @Override
    public Logger log() {
        return LOG;
    }

}
