package cuenen.raymond.messaging.client;

import cuenen.raymond.messaging.BrokerProperties;
import static cuenen.raymond.messaging.BrokerProperties.*;
import cuenen.raymond.messaging.Client;
import cuenen.raymond.messaging.MessageConverter;
import cuenen.raymond.messaging.Version;
import java.util.HashMap;
import java.util.Map;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ActiveMQ client is used to connect to the ActiveMQ message broker.
 */
public class ActiveMQClient extends JMSClient {

    private static final Logger LOG = LoggerFactory.getLogger(ActiveMQClient.class);
    private static final Map<String, String> DEFAULTS = new HashMap<>();

    static {
        LOG.info(Version.Current.toString());
        DEFAULTS.put(BROKER_URL, ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        DEFAULTS.put(USERNAME, ActiveMQConnectionFactory.DEFAULT_USER);
        DEFAULTS.put(PASSWORD, ActiveMQConnectionFactory.DEFAULT_PASSWORD);
    }

    /**
     * Creates a new {@link ActiveMQClient}.
     *
     * @param messageConverter The message converter.
     * @param properties The broker properties.
     * @return The client implementation for ActiveMQ.
     */
    public static Client create(MessageConverter messageConverter, BrokerProperties properties) {
        properties.setDefaults(DEFAULTS);
        final ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(properties.getStringProperty(BROKER_URL));
        connectionFactory.setUserName(properties.getStringProperty(USERNAME));
        connectionFactory.setPassword(properties.getStringProperty(PASSWORD));
        return new ActiveMQClient(messageConverter, connectionFactory);
    }

    /**
     * Creates a new {@link ActiveMQClient}.
     *
     * @param messageConverter The {@link MessageConverter} for encoding and
     * decoding messages.
     * @param connectionFactory The connection factory for opening a
     * {@link Connection} to the message broker.
     */
    public ActiveMQClient(MessageConverter messageConverter, ConnectionFactory connectionFactory) {
        super(messageConverter, connectionFactory);
    }

    @Override
    public Logger log() {
        return LOG;
    }

}
