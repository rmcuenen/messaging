package cuenen.raymond.messaging;

import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * This class encapsulates the information needed to perform an asynchronous
 * request-response using message queues.
 */
public final class Request {

    /**
     * The default reply-timeout; 2 seconds.
     */
    public static final Timeout DefaultReplyTimeout = Timeout.create(2, TimeUnit.SECONDS);

    /**
     * Creates a new {@link Request} object, using a random correlation
     * identifier and the default reply-timeout.
     *
     * @param queueName The queue name to send the request to.
     * @param message The message object to send.
     * @return The request.
     */
    public static Request create(String queueName, DataMessage message) {
        return new Request(queueName, message, createCorrelationId(), DefaultReplyTimeout);
    }

    /**
     * Creates a new {@link Request} object, using the default reply-timeout.
     *
     * @param queueName The queue name to send the request to.
     * @param message The message object to send.
     * @param correlationId The correlation identifier of the message.
     * @return The request.
     */
    public static Request create(String queueName, DataMessage message, long correlationId) {
        return new Request(queueName, message, correlationId, DefaultReplyTimeout);
    }

    /**
     * Creates a new {@link Request} object, using a random correlation
     * identifier.
     *
     * @param queueName The queue name to send the request to.
     * @param message The message object to send.
     * @param replyTimeout The timeout to wait for a reply.
     * @return The request.
     */
    public static Request create(String queueName, DataMessage message, Timeout replyTimeout) {
        return new Request(queueName, message, createCorrelationId(), replyTimeout);
    }

    /**
     * Creates a new {@link Request} object.
     *
     * @param queueName The queue name to send the request to.
     * @param message The message object to send.
     * @param correlationId The correlation identifier of the message.
     * @param replyTimeout The timeout to wait for a reply.
     * @return The request.
     */
    public static Request create(String queueName, DataMessage message,
            long correlationId, Timeout replyTimeout) {
        return new Request(queueName, message, correlationId, replyTimeout);
    }

    /**
     * Creates a random correlation identifier.
     *
     * @return A random correlation identifier.
     */
    private static long createCorrelationId() {
        final Random random = new Random(System.currentTimeMillis());
        final int randomInt = random.nextInt();
        return randomInt & 0x00000000ffffffffL;
    }

    private final String queueName;
    private final DataMessage message;
    private final long correlationId;
    private final Timeout replyTimeout;

    /**
     * Creates a new {@link Request} object.
     *
     * @param queueName The queue name to send the request to.
     * @param message The message object to send.
     * @param correlationId The correlation identifier of the message.
     * @param replyTimeout The timeout to wait for a reply.
     */
    public Request(String queueName, DataMessage message,
            long correlationId, Timeout replyTimeout) {
        this.queueName = queueName;
        this.message = message;
        this.correlationId = correlationId;
        this.replyTimeout = replyTimeout;
    }

    /**
     * Returns the queue name to send the request to.
     *
     * @return The queue name.
     */
    public String getQueueName() {
        return queueName;
    }

    /**
     * Returns the message object to send.
     *
     * @return The message object.
     */
    public DataMessage getMessage() {
        return message;
    }

    /**
     * Returns the correlation identifier of the message.
     *
     * @return The correlation identifier.
     */
    public long getCorrelationId() {
        return correlationId;
    }

    /**
     * Returns the timeout to wait for a reply.
     *
     * @return The reply-timeout.
     */
    public Timeout getReplyTimeout() {
        return replyTimeout;
    }

    @Override
    public int hashCode() {
        return 41 * (41 * (41 * (41 + queueName.hashCode()) + message.hashCode())
                + Objects.hashCode(correlationId)) + replyTimeout.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Request) {
            Request that = (Request) obj;
            return this.queueName.equals(that.queueName)
                    && this.message.equals(that.message)
                    && Long.compare(this.correlationId, that.correlationId) == 0
                    && this.replyTimeout.equals(that.replyTimeout);
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("Request(%s, %s, %d, %s)", queueName, message,
                correlationId, replyTimeout);
    }
}
