package cuenen.raymond.messaging;

/**
 * This exception is thrown when a property is requested that is not contained
 * within the message.
 */
public class MessagePropertyNotFoundException extends DataMessageException {

    /**
     * Constructs a {@link MessagePropertyNotFoundException} with the specified
     * reason.
     *
     * @param reason A description of the exception.
     */
    public MessagePropertyNotFoundException(String reason) {
        super(reason);
    }
}
