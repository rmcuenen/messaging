package cuenen.raymond.messaging;

import java.util.Enumeration;
import java.util.Objects;

/**
 * Utility class for data messages. This class provides the means to calculate
 * the hash code of data messages. It also provides an equals method to
 * determine equality between two data messages.
 */
public final class DataMessageUtil {

    /**
     * Calculates a hash for the given DataMessage.
     * <p>
     * Hash values are calculated according Austin Appleby's MurmurHash 3
     * algorithm. The final hash value is computed by mixing the Java hash
     * values of all the message properties. Only when the message does not
     * contain any properties the Java hash value of the message type is
     * returned.
     *
     * @param message The DataMessage to calculate the hash for.
     * @return The calculated hash value.
     */
    public static int hashCode(DataMessage message) {
        int arr = 0;
        int hash = 0xcafebabe;
        for (Enumeration<String> names = message.getPropertyNames(); names.hasMoreElements();) {
            final String name = names.nextElement();
            arr++;
            try {
                final Object elem = message.getObjectProperty(name);
                int k = Objects.hashCode(elem);
                k *= 0xcc9e2d51;
                k = Integer.rotateLeft(k, 15);
                k *= 0x1b873593;
                int h = hash ^ k;
                h = Integer.rotateLeft(h, 13);
                hash = h * 5 + 0xe6546b64;
            } catch (DataMessageException ex) {
                // Should not happen
                throw new InternalError(ex.getMessage());
            }
        }
        if (arr == 0) {
            return message.getMessageType().hashCode();
        }
        int h = hash ^ arr;
        h ^= h >>> 16;
        h *= 0x85ebca6b;
        h ^= h >>> 13;
        h *= 0xc2b2ae35;
        h ^= h >>> 16;
        return h;
    }

    /**
     * Tests whether two DataMessages are equal to each other.
     * <p>
     * This is implemented by a three-stage fast-fail method:
     * <ol>
     * <li>Check that the messages are of the same type.<br><i>Fails with
     * different message types.</i></li>
     * <li>Check that all the property names of the first message exist in the
     * second message.<br><i>Fails when the first message has more properties,
     * or properties with a different name then the second message.</i></li>
     * <li>Check that all the property values of the second message match the
     * property values of the first message.<br><i>Fails when the second message
     * has more properties then the first message, or when a property has a
     * different value.</i></li>
     * </ol>
     *
     * @param first The first DataMessage to compare.
     * @param second The second DataMessage to compare.
     * @return {@code true} when the DataMessages are equal to each other,
     * {@code false} otherwise.
     */
    public static boolean equals(DataMessage first, DataMessage second) {
        // Messages of the same type?
        if (!first.getMessageType().equals(second.getMessageType())) {
            return false;
        }
        // Does message 2 have all the properties message 1 has?
        for (Enumeration<String> names = first.getPropertyNames(); names.hasMoreElements();) {
            final String name = names.nextElement();
            if (!second.propertyExists(name)) {
                return false;
            }
        }
        // Are all the properties of message 2 equal to the properties of message 1?
        for (Enumeration<String> names = second.getPropertyNames(); names.hasMoreElements();) {
            final String name = names.nextElement();
            if (!first.propertyExists(name)) {
                return false;
            }
            try {
                if (!Objects.equals(first.getObjectProperty(name), second.getObjectProperty(name))) {
                    return false;
                }
            } catch (DataMessageException ex) {
                return false;
            }
        }
        return true;
    }
    
    private DataMessageUtil() {
        // Utilty class
    }
}
