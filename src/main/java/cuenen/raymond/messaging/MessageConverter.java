package cuenen.raymond.messaging;

import cuenen.raymond.messaging.transport.JavaSerializer;

/**
 * The message converter allows a client to decode and encode messages to and
 * from a byte-array.
 */
public interface MessageConverter {

    MessageConverter DefaultMessageConverter = new JavaSerializer();

    /**
     * Decode a byte-array into the message object indicated by the message
     * type.
     *
     * @param messageType The message type.
     * @param message The encoded message.
     * @return The decoded message object.
     * @throws java.lang.Exception When decoding fails.
     */
    DataMessage decode(String messageType, byte[] message) throws Exception;

    /**
     * Encode a message object into a byte-array.
     *
     * @param message The decoded message object.
     * @return The encoded message.
     * @throws java.lang.Exception When encoding fails.
     */
    byte[] encode(DataMessage message) throws Exception;
}
