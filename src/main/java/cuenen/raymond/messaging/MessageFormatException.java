package cuenen.raymond.messaging;

/**
 * This exception is thrown when an attempt is made to read a message property
 * as the wrong type.
 */
public class MessageFormatException extends DataMessageException {

    /**
     * Constructs a {@link MessageFormatException} with the specified reason.
     *
     * @param reason A description of the exception.
     */
    public MessageFormatException(String reason) {
        super(reason);
    }
}
