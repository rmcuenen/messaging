package cuenen.raymond.messaging;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * A message selector allows a client to specify, by header field references and
 * property references, the messages it is interested in. Only messages whose
 * header and property values match the selector are delivered.
 * <p>
 * A message selector can be retrieved through a
 * {@link MessageSelector.MessageSelectorBuilder}.
 *
 * @see MessageSelector.MessageSelectorBuilder#build()
 */
public class MessageSelector {

    private final Collection<HeaderProperty> arguments = new HashSet<>();
    private final XMatch matcher;

    /**
     * Creates a new {@link MessageSelector}.
     *
     * @param matcher The {@code x-match} argument.
     * @param arguments The message properties to match against.
     */
    public MessageSelector(XMatch matcher, Collection<HeaderProperty> arguments) {
        this.matcher = matcher;
        this.arguments.addAll(arguments);
    }

    /**
     * Constructs an argument table for use with the AMQP headers exchange.
     * <p>
     * A field in the bind arguments matches a field in the message if either
     * the field in the bind arguments has no value and a field of the same name
     * is present in the message headers or if the field in the bind arguments
     * has a value and a field of the same name exists in the message headers
     * and has that same value.
     *
     * @return The bind arguments.
     */
    public Map<String, Object> toBindArguments() {
        final Map<String, Object> table = new HashMap<>();
        table.put(XMatch.Argument, matcher.value());
        arguments.stream().forEach(
                property -> table.put(property.getName(), property.getValue()));
        return table;
    }

    /**
     * Constructs a SQL92 conditional expression syntax based {@code String} for
     * use with JMS.
     * <p>
     * The constructed expression only checks for equality of header values.
     * Note that the IS NULL operator tests for a null header field value or a
     * missing property value.
     *
     * @return A JMS message selector {@code String}.
     */
    public String toExpression() {
        final StringBuilder sb = new StringBuilder();
        boolean next = false;
        for (HeaderProperty property : arguments) {
            if (next) {
                sb.append(' ').append(matcher.operator()).append(' ');
            }
            sb.append(property.toExpression());
            next = true;
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return 41 * (41 + matcher.hashCode()) + arguments.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MessageSelector) {
            MessageSelector that = (MessageSelector) obj;
            return this.matcher == that.matcher
                    && this.arguments.equals(that.arguments);
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("MessageSelector(%s, %s)", matcher, arguments);
    }

    /**
     * Builder for creating a message selector.
     */
    public static class MessageSelectorBuilder {

        /**
         * A convenient factory method to create a message selector builder
         * matching all the message header values.
         *
         * @return The builder.
         */
        public static MessageSelectorBuilder allMessageSelector() {
            return new MessageSelectorBuilder().matcher(XMatch.All);
        }

        /**
         * A convenient factory method to create a message selector builder
         * matching any of the message header values.
         *
         * @return The builder.
         */
        public static MessageSelectorBuilder anyMessageSelector() {
            return new MessageSelectorBuilder().matcher(XMatch.Any);
        }

        /**
         * A convenient factory method to create a message selector builder.
         *
         * @return The builder.
         */
        public static MessageSelectorBuilder messageSelector() {
            return new MessageSelectorBuilder();
        }

        private final Map<String, HeaderProperty> argumentTable = new HashMap<>();
        private XMatch matcher = XMatch.All;

        /**
         * Sets the matcher type to use for the message selector.
         *
         * @param matcher The {@code x-match} argument.
         * @return The builder.
         */
        public MessageSelectorBuilder matcher(XMatch matcher) {
            this.matcher = matcher;
            return this;
        }

        /**
         * Sets a {@code boolean} property value with the specified name into
         * the message selector.
         *
         * @param name The name of the {@code boolean} property.
         * @param value The {@code boolean} property value to set.
         * @return The builder.
         */
        public MessageSelectorBuilder setBooleanProperty(String name, boolean value) {
            argumentTable.put(name, HeaderProperty.createBooleanProperty(name, value));
            return this;
        }

        /**
         * Sets a {@code byte} property value with the specified name into the
         * message selector.
         *
         * @param name The name of the {@code byte} property.
         * @param value The {@code byte} property value to set.
         * @return The builder.
         */
        public MessageSelectorBuilder setByteProperty(String name, byte value) {
            argumentTable.put(name, HeaderProperty.createNumberProperty(name, value));
            return this;
        }

        /**
         * Sets a {@code short} property value with the specified name into the
         * message selector.
         *
         * @param name The name of the {@code short} property.
         * @param value The {@code short} property value to set.
         * @return The builder.
         */
        public MessageSelectorBuilder setShortProperty(String name, short value) {
            argumentTable.put(name, HeaderProperty.createNumberProperty(name, value));
            return this;
        }

        /**
         * Sets a {@code int} property value with the specified name into the
         * message selector.
         *
         * @param name The name of the {@code int} property.
         * @param value The {@code int} property value to set.
         * @return The builder.
         */
        public MessageSelectorBuilder setIntProperty(String name, int value) {
            argumentTable.put(name, HeaderProperty.createNumberProperty(name, value));
            return this;
        }

        /**
         * Sets a {@code long} property value with the specified name into the
         * message selector.
         *
         * @param name The name of the {@code long} property.
         * @param value The {@code long} property value to set.
         * @return The builder.
         */
        public MessageSelectorBuilder setLongProperty(String name, long value) {
            argumentTable.put(name, HeaderProperty.createNumberProperty(name, value));
            return this;
        }

        /**
         * Sets a {@code float} property value with the specified name into the
         * message selector.
         *
         * @param name The name of the {@code float} property.
         * @param value The {@code float} property value to set.
         * @return The builder.
         */
        public MessageSelectorBuilder setFloatProperty(String name, float value) {
            argumentTable.put(name, HeaderProperty.createNumberProperty(name, value));
            return this;
        }

        /**
         * Sets a {@code double} property value with the specified name into the
         * message selector.
         *
         * @param name The name of the {@code double} property.
         * @param value The {@code double} property value to set.
         * @return The builder.
         */
        public MessageSelectorBuilder setDoubleProperty(String name, double value) {
            argumentTable.put(name, HeaderProperty.createNumberProperty(name, value));
            return this;
        }

        /**
         * Sets a {@code String} property value with the specified name into the
         * message selector.
         *
         * @param name The name of the {@code String} property.
         * @param value The {@code String} property value to set.
         * @return The builder.
         */
        public MessageSelectorBuilder setStringProperty(String name, String value) {
            argumentTable.put(name, HeaderProperty.createStringProperty(name, value));
            return this;
        }

        /**
         * Sets a valueless property with the specified name into the message
         * selector.
         *
         * @param name The name of the property.
         * @return The builder.
         */
        public MessageSelectorBuilder setNullProperty(String name) {
            argumentTable.put(name, HeaderProperty.createNullProperty(name));
            return this;
        }

        /**
         * Build the message selector.
         *
         * @return The message selector.
         */
        public MessageSelector build() {
            if (matcher == null) {
                throw new IllegalStateException("matcher type is not set");
            }
            return new MessageSelector(matcher, argumentTable.values());
        }
    }
}
