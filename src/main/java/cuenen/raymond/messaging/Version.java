package cuenen.raymond.messaging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * This class holds the version and build information of the Message Client
 * Library.
 */
public final class Version {

    /**
     * Current release version.
     */
    public static final Version Current;

    static {
        String version = "UNVERSIONED";
        String build = "NA";
        String timestamp = "NA";

        try {
            final Properties props = new Properties();
            props.load(Version.class.getResourceAsStream("/version.properties"));
            version = props.getProperty("version", version);
            build = props.getProperty("build", build);
            final String gitTimestampRaw = props.getProperty("timestamp");
            if (gitTimestampRaw != null) {
                final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                timestamp = formatter.format(new Date(Long.parseLong(gitTimestampRaw)));
            }
        } catch (Exception ex) {
            // Ignore
        }

        Current = new Version(version, build, timestamp);
    }

    private final String version;
    private final String build;
    private final String timestamp;

    private Version(String version, String build, String timestamp) {
        this.version = version;
        this.build = build;
        this.timestamp = timestamp;
    }

    /**
     * Returns the version of the Message Client Library. The format is
     * {@code "major.minor.bugfix"}.
     *
     * @return The version number.
     */
    public String version() {
        return version;
    }

    /**
     * Returns the build number of the Message Client Library. This information
     * is useful when reporting bugs.
     *
     * @return The build number.
     */
    public String build() {
        return build;
    }

    /**
     * Returns the build timestamp of the Message Client Library.
     *
     * @return The build timestamp.
     */
    public String timestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return String.format("Message Client Library %s (Build %s, %s)", version, build, timestamp);
    }
}
