package cuenen.raymond.messaging;

import static cuenen.raymond.messaging.BrokerProperties.*;
import static cuenen.raymond.messaging.MessageConverter.DefaultMessageConverter;
import java.util.concurrent.CompletableFuture;

/**
 * A client provides a one stop interface for performing actions on the message
 * queue.
 * <p>
 * All operations performed are asynchronous by nature. Each send action
 * therefore returns a {@link java.util.concurrent.CompletableFuture} holding
 * the response when it becomes available.
 * <p>
 * A client can be retrieved through a {@link Client.ClientBuilder}.
 *
 * @see Client.ClientBuilder#build()
 */
public interface Client extends AutoCloseable {

    /**
     * Message header property name indicating the message type.
     */
    String MessageTypeHeader = "MessageType";

    /**
     * Connect to the message broker. If the client is already connected, this
     * method is no-op.
     *
     * @throws Exception If a problem is encountered.
     */
    void connect() throws Exception;

    /**
     * Subscribe to messages published to a topic.
     *
     * @param subscriber The subscriber information.
     */
    void subscribe(Subscriber subscriber);

    /**
     * Un-subscribe a subscriber to stop receiving messages from a topic.
     *
     * @param subscriber The subscriber information.
     */
    void unsubscribe(Subscriber subscriber);

    /**
     * Publish a message to a topic.
     *
     * @param topicName The topic name to publish to.
     * @param data The message object to publish.
     * @throws Exception If a problem is encountered.
     */
    void publish(String topicName, DataMessage data) throws Exception;

    /**
     * Perform an asynchronous request-response.
     *
     * @param request The request to be send.
     * @return A Future that will provide the result and the status of the
     * response.
     */
    CompletableFuture<? extends DataMessage> send(Request request);

    /**
     * Builder for creating a client.
     */
    public static class ClientBuilder {

        private final BrokerProperties properties = new BrokerProperties();
        private MessageConverter converter = DefaultMessageConverter;
        private MessageBroker broker;

        /**
         * A convenient factory method to create a client builder.
         *
         * @return The builder.
         */
        public static ClientBuilder clientBuilder() {
            return new ClientBuilder();
        }

        /**
         * Sets the message broker to use for the client.
         *
         * @param broker The message broker.
         * @return The builder.
         */
        public ClientBuilder broker(MessageBroker broker) {
            this.broker = broker;
            return this;
        }

        /**
         * Sets the message converter to use for the client.
         *
         * @param converter The message converter.
         * @return The builder.
         */
        public ClientBuilder converter(MessageConverter converter) {
            this.converter = converter;
            return this;
        }

        /**
         * Sets the connection URL used to connect to the message broker.
         *
         * @param brokerURL The transport URI.
         * @return The builder.
         */
        public ClientBuilder connection(String brokerURL) {
            properties.setStringProperty(BROKER_URL, brokerURL);
            return this;
        }

        /**
         * Sets the user name to use when connecting to the message broker.
         *
         * @param username The user name.
         * @return The builder.
         */
        public ClientBuilder user(String username) {
            properties.setStringProperty(USERNAME, username);
            return this;
        }

        /**
         * Sets the password to use when connecting to the message broker.
         *
         * @param password The password.
         * @return The builder.
         */
        public ClientBuilder password(String password) {
            properties.setStringProperty(PASSWORD, password);
            return this;
        }

        /**
         * Sets a boolean property value with the specified name.
         *
         * @param name The property name.
         * @param value The property value.
         * @return The builder.
         */
        public ClientBuilder setBooleanProperty(String name, Boolean value) {
            properties.setBooleanProperty(name, value);
            return this;
        }

        /**
         * Sets a number property value with the specified name.
         *
         * @param name The property name.
         * @param value The property value.
         * @return The builder.
         */
        public ClientBuilder setNumberProperty(String name, Number value) {
            properties.setNumberProperty(name, value);
            return this;
        }

        /**
         * Sets a string property value with the specified name.
         *
         * @param name The property name.
         * @param value The property value.
         * @return The builder.
         */
        public ClientBuilder setStringProperty(String name, String value) {
            properties.setStringProperty(name, value);
            return this;
        }

        /**
         * Sets an object property value with the specified name.
         *
         * @param name The property name.
         * @param value The property value.
         * @return The builder.
         */
        public ClientBuilder setObjectProperty(String name, Object value) {
            properties.setObjectProperty(name, value);
            return this;
        }

        /**
         * Build the client.
         *
         * @return The client implementation.
         * @throws java.lang.IllegalStateException When no message converter is
         * set.
         */
        public Client build() {
            if (converter == null) {
                throw new IllegalStateException("No message converter set");
            }
            return broker.client(converter, properties);
        }
    }
}
