package cuenen.raymond.messaging;

import java.util.Enumeration;

/**
 * A data message is the basis for all the messages being send and received
 * through a client. It defines the message type that identifies the message for
 * encoding and decoding. The message metadata is provided by means of
 * properties.
 */
public interface DataMessage {

    /**
     * Returns the type of the message.
     *
     * @return The message type.
     */
    String getMessageType();

    /**
     * Marshall operation event listener. Used to set the message to read-only.
     */
    void onMarshall();

    /**
     * Indicates whether a property value exists.
     *
     * @param name The name of the property to test.
     * @return {@code true} when the property exists, {@code false} otherwise.
     */
    boolean propertyExists(String name);

    /**
     * Returns an {@link Enumeration} of all the property names.
     *
     * @return An enumeration of all the names of property values.
     */
    Enumeration<String> getPropertyNames();

    /**
     * Clears a message's properties.
     */
    void clearProperties();

    /**
     * Returns the value of the {@code Boolean} property with the specified
     * name.
     *
     * @param name The name of the {@code Boolean} property.
     * @return The {@code Boolean} property value for the specified name.
     * @throws DataMessageException When it fails to get the property value.
     * @throws MessagePropertyNotFoundException When the property does not
     * exist.
     * @throws MessageFormatException When the property value cannot be
     * converted into a {@code Boolean}.
     */
    Boolean getBooleanProperty(String name) throws DataMessageException;

    /**
     * Returns the value of the {@code Number} property with the specified name.
     *
     * @param name The name of the {@code Number} property.
     * @return The {@code Number} property value for the specified name.
     * @throws DataMessageException When it fails to get the property value.
     * @throws MessagePropertyNotFoundException When the property does not
     * exist.
     * @throws MessageFormatException When the property value cannot be
     * converted into a {@code Number}.
     */
    Number getNumberProperty(String name) throws DataMessageException;

    /**
     * Returns the value of the {@code String} property with the specified name.
     *
     * @param name The name of the {@code String} property.
     * @return The {@code String} property value for the specified name.
     * @throws DataMessageException When it fails to get the property value.
     * @throws MessagePropertyNotFoundException When the property does not
     * exist.
     * @throws MessageFormatException When the property value cannot be
     * converted into a {@code String}.
     */
    String getStringProperty(String name) throws DataMessageException;

    /**
     * Returns the value of the Java object property with the specified name.
     *
     * @param name The name of the Java object property.
     * @return The Java object property value with the specified name.
     * @throws DataMessageException When it fails to get the property value.
     * @throws MessagePropertyNotFoundException When the property does not
     * exist.
     */
    Object getObjectProperty(String name) throws DataMessageException;

    /**
     * Sets a {@code Boolean} property value with the specified name into the
     * message.
     *
     * @param name The name of the {@code Boolean} property.
     * @param value The {@code Boolean} property value to set.
     * @throws DataMessageException When it fails to set the property.
     * @throws MessageNotWriteableException When the properties are read-only.
     */
    void setBooleanProperty(String name, Boolean value) throws DataMessageException;

    /**
     * Sets a {@code Number} property value with the specified name into the
     * message.
     *
     * @param name The name of the {@code Number} property.
     * @param value The {@code Number} property value to set.
     * @throws DataMessageException When it fails to set the property.
     * @throws MessageNotWriteableException When the properties are read-only.
     */
    void setNumberProperty(String name, Number value) throws DataMessageException;

    /**
     * Sets a {@code String} property value with the specified name into the
     * message.
     *
     * @param name The name of the {@code String} property.
     * @param value The {@code String} property value to set.
     * @throws DataMessageException When it fails to set the property.
     * @throws MessageNotWriteableException When the properties are read-only.
     */
    void setStringProperty(String name, String value) throws DataMessageException;

    /**
     * Sets a Java object property value with the specified name into the
     * message.
     * <p>
     * Note that this method works only for the object types {@code Boolean},
     * {@code Number} and {@code String}.
     *
     * @param name The name of the Java object property.
     * @param value The Java object property value to set.
     * @throws DataMessageException When it fails to set the property.
     * @throws IllegalArgumentException When the name is null or an empty
     * string.
     * @throws MessageFormatException When the object is invalid.
     * @throws MessageNotWriteableException When the properties are read-only.
     */
    void setObjectProperty(String name, Object value) throws DataMessageException;
}
