package cuenen.raymond.messaging;

import java.util.Objects;

/**
 * This class represents a message header name and value pair to be used with a
 * message selector.
 */
public abstract class HeaderProperty {

    private static final String EqualTo = " = ";

    /**
     * Creates a new header property with a boolean typed value.
     *
     * @param name The name of the property.
     * @param value The value of the property.
     * @return The message property.
     */
    public static HeaderProperty createBooleanProperty(String name, final Boolean value) {
        if (value == null) {
            throw new NullPointerException("Boolean value is null");
        }
        return new HeaderProperty(name, value) {

            @Override
            public String toExpression() {
                return getName() + EqualTo + value;
            }
        };
    }

    /**
     * Creates a new header property with a number typed value.
     *
     * @param name The name of the property.
     * @param value The value of the property.
     * @return The message property.
     */
    public static HeaderProperty createNumberProperty(String name, final Number value) {
        if (value == null) {
            throw new NullPointerException("Number value is null");
        }
        return new HeaderProperty(name, value) {

            @Override
            public String toExpression() {
                return getName() + EqualTo + value;
            }
        };
    }

    /**
     * Creates a new header property with a string typed value.
     *
     * @param name The name of the property.
     * @param value The value of the property.
     * @return The message property.
     */
    public static HeaderProperty createStringProperty(String name, final String value) {
        if (value == null) {
            throw new NullPointerException("String value is null");
        }
        return new HeaderProperty(name, value) {

            @Override
            public String toExpression() {
                return getName() + EqualTo + escape(value);
            }

            private String escape(String str) {
                final StringBuilder sb = new StringBuilder();
                sb.append('\'');
                for (int i = 0; i < str.length(); i++) {
                    final char c = str.charAt(i);
                    sb.append(c);
                    if (c == '\'') {
                        sb.append(c);
                    }
                }
                sb.append('\'');
                return sb.toString();
            }
        };
    }

    /**
     * Creates a new header property with no value.
     *
     * @param name The name of the property.
     * @return The message property.
     */
    public static HeaderProperty createNullProperty(String name) {
        return new HeaderProperty(name, null) {

            @Override
            public String toExpression() {
                return getName() + " IS NOT NULL";
            }
        };
    }

    private final String name;
    private final Object value;

    /**
     * Creates a new {@link HeaderProperty}.
     *
     * @param name The name of the property.
     * @param value The value of the property.
     */
    public HeaderProperty(String name, Object value) {
        if (name == null || name.equals("")) {
            throw new IllegalArgumentException("Property name cannot be empty or null");
        }
        this.name = name;
        this.value = value;
    }

    /**
     * Returns the name of this header property.
     *
     * @return The name of the property;
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the value of this header property. For properties with no value
     * this returns {@code null}.
     *
     * @return The value of the property.
     */
    public Object getValue() {
        return value;
    }

    /**
     * Constructs a SQL92 based conditional expression representing this header
     * property.
     *
     * @return The SQL92 based conditional expression string.
     */
    public abstract String toExpression();

    @Override
    public int hashCode() {
        return 41 * (41 + name.hashCode()) + Objects.hashCode(value);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof HeaderProperty) {
            HeaderProperty that = (HeaderProperty) obj;
            return this.name.equals(that.name)
                    && Objects.equals(this.value, that.value);
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("HeaderProperty(%s, %s)", name, value == null ? "Any" : value);
    }
}
