package cuenen.raymond.messaging;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import static java.util.concurrent.TimeUnit.*;

/**
 * Utility for working with {@link java.util.concurrent.TimeUnit} durations.
 * This class is not meant as a general purpose representation of time.
 */
public class Timeout implements Serializable, Comparable<Timeout> {

    /**
     * Construct a Timeout from the given length and unit. Observe that
     * nanosecond precision may be lost if
     * <ul>
     * <li>the unit is NANOSECONDS
     * <li>and the length has a value greater than 2^53
     * </ul>
     *
     * @param length The length.
     * @param unit The unit.
     * @return The timeout object.
     * @throws IllegalArgumentException if the length was finite but the
     * resulting timeout cannot be expressed as a {@link Timeout}.
     */
    public static Timeout create(double length, TimeUnit unit) {
        return fromNanos(unit.toNanos(1) * length);
    }

    /**
     * Construct a timeout from the given length and time unit.
     *
     * @param length The length.
     * @param unit The unit.
     * @return The timeout object.
     */
    public static Timeout create(long length, TimeUnit unit) {
        return new Timeout(length, unit);
    }

    /**
     * Construct a timeout from the given length and time unit, where the latter
     * is looked up in a list of string representation. Valid choices are:
     * <p>
     * `d, day, h, hour, min, minute, s, sec, second, ms, milli, millisecond,
     * µs, micro, microsecond, ns, nano, nanosecond` and their pluralized forms
     * (for every but the first mentioned form of each unit, i.e. no "ds", but
     * "days").
     *
     * @param length The length.
     * @param unit The unit.
     * @return The timeout object.
     */
    public static Timeout create(long length, String unit) {
        return new Timeout(length, Timeout.timeUnit.get(unit));
    }

    // Double stores 52 bits mantissa, but there is an implied '1' in front, making the limit 2^53
    private static final double maxPreciseDouble = 9007199254740992d;

    /**
     * Parse String into Timeout. Format is `"&lt;length&gt;&lt;unit&gt;"`,
     * where whitespace is allowed before, between and after the parts.
     *
     * @param s The string to parse.
     * @return The timeout object.
     * @throws NumberFormatException if format is not parseable
     */
    public static Timeout create(String s) {
        final StringBuilder s1 = new StringBuilder(s.replaceAll("\\s+", ""));
        final StringBuilder unitName = new StringBuilder();
        s1.reverse();
        int index = 0;
        while (Character.isLetter(s1.charAt(index))) {
            unitName.append(s1.charAt(index++));
        }
        unitName.reverse();
        final TimeUnit unit = timeUnit.get(unitName.toString());
        if (unit == null) {
            throw new NumberFormatException("format error " + s);
        }
        final String valueStr = s1.reverse().toString().replace(unitName, "");
        final double valueD = Double.parseDouble(valueStr);
        if (valueD >= -maxPreciseDouble && valueD <= maxPreciseDouble) {
            return create(valueD, unit);
        } else {
            return create(Long.parseLong(valueStr), unit);
        }
    }

    private static List<String> words(String s) {
        return new ArrayList<>(Arrays.asList(s.trim().split("\\s+")));
    }

    private static List<String> expandLabels(String labels) {
        final List<String> rest = words(labels);
        final String hd = rest.remove(0);
        final List<String> result = new ArrayList<>();
        result.add(hd);
        for (String s : rest) {
            result.add(s);
            result.add(s + "s");
        }
        return result;
    }

    private static final Map<TimeUnit, String> timeUnitLabels = new HashMap<>();

    static {
        timeUnitLabels.put(DAYS, "d day");
        timeUnitLabels.put(HOURS, "h hour");
        timeUnitLabels.put(MINUTES, "min minute");
        timeUnitLabels.put(SECONDS, "s sec second");
        timeUnitLabels.put(MILLISECONDS, "ms milli millisecond");
        timeUnitLabels.put(MICROSECONDS, "µs micro microsecond");
        timeUnitLabels.put(NANOSECONDS, "ns nano nanosecond");
    }

    protected static final Map<TimeUnit, String> timeUnitName = new HashMap<>();

    static {
        for (Map.Entry<TimeUnit, String> entry : timeUnitLabels.entrySet()) {
            final List<String> names = words(entry.getValue());
            timeUnitName.put(entry.getKey(), names.get(names.size() - 1));
        }
    }

    protected static final Map<String, TimeUnit> timeUnit = new HashMap<>();

    static {
        for (Map.Entry<TimeUnit, String> entry : timeUnitLabels.entrySet()) {
            List<String> names = expandLabels(entry.getValue());
            for (String name : names) {
                timeUnit.put(name, entry.getKey());
            }
        }
    }

    /**
     * Construct a Timeout from the given number of nanoseconds.
     *
     * @param nanos The number of nanoseconds.
     * @return The timeout object.
     * @throws IllegalArgumentException if the length was finite but the
     * resulting timeout cannot be expressed as a {@link Timeout}
     */
    public static Timeout fromNanos(double nanos) {
        if (nanos > Long.MAX_VALUE || nanos < Long.MIN_VALUE) {
            throw new IllegalArgumentException("trying to construct too large timeout with " + nanos + "ns");
        } else {
            return fromNanos((long) (nanos + 0.5));
        }
    }

    private static final long µs_per_ns = 1000L;
    private static final long ms_per_ns = µs_per_ns * 1000;
    private static final long s_per_ns = ms_per_ns * 1000;
    private static final long min_per_ns = s_per_ns * 60;
    private static final long h_per_ns = min_per_ns * 60;
    private static final long d_per_ns = h_per_ns * 24;

    /**
     * Construct a timeout from the given number of nanoseconds. The result will
     * have the coarsest possible time unit which can exactly express this
     * timeout.
     *
     * @param nanos The number of nanoseconds.
     * @return The timeout object.
     * @throws IllegalArgumentException for `negative` values
     */
    public static Timeout fromNanos(long nanos) {
        if (nanos % d_per_ns == 0) {
            return create(nanos / d_per_ns, DAYS);
        } else if (nanos % h_per_ns == 0) {
            return create(nanos / h_per_ns, HOURS);
        } else if (nanos % min_per_ns == 0) {
            return create(nanos / min_per_ns, MINUTES);
        } else if (nanos % s_per_ns == 0) {
            return create(nanos / s_per_ns, SECONDS);
        } else if (nanos % ms_per_ns == 0) {
            return create(nanos / ms_per_ns, MILLISECONDS);
        } else if (nanos % µs_per_ns == 0) {
            return create(nanos / µs_per_ns, MICROSECONDS);
        } else {
            return create(nanos, NANOSECONDS);
        }
    }

    // limit on abs. value of timeouts in their units
    private static final long max_ns = Long.MAX_VALUE;
    private static final long max_µs = max_ns / 1000;
    private static final long max_ms = max_µs / 1000;
    private static final long max_s = max_ms / 1000;
    private static final long max_min = max_s / 60;
    private static final long max_h = max_min / 60;
    private static final long max_d = max_h / 24;

    private final long length;
    private final TimeUnit unit;

    public Timeout(long length, TimeUnit unit) {
        this.length = length;
        this.unit = unit;
        // enforce the 2^63-1 ns limit.
        if (!require()) {
            throw new IllegalArgumentException("Timeout is limited to 2^63-1 ns (ca. 292 years)");
        }
    }

    public long length() {
        return length;
    }

    public TimeUnit unit() {
        return unit;
    }

    private boolean bounded(long max) {
        return 0 <= length && length <= max;
    }

    private boolean require() {
        switch (unit) {
            case NANOSECONDS:
                return bounded(max_ns);
            case MICROSECONDS:
                return bounded(max_µs);
            case MILLISECONDS:
                return bounded(max_ms);
            case SECONDS:
                return bounded(max_s);
            case MINUTES:
                return bounded(max_min);
            case HOURS:
                return bounded(max_h);
            case DAYS:
                return bounded(max_d);
            default:
                final long v = DAYS.convert(length, unit);
                return -max_d <= v && v <= max_d;
        }
    }

    /**
     * Return the length of this timeout measured in whole nanoseconds, rounding
     * towards zero.
     *
     * @return The number of nanoseconds.
     */
    public long toNanos() {
        return unit.toNanos(length);
    }

    /**
     * Return the length of this timeout measured in whole microseconds,
     * rounding towards zero.
     *
     * @return The number of microseconds.
     */
    public long toMicros() {
        return unit.toMicros(length);
    }

    /**
     * Return the length of this timeout measured in whole milliseconds,
     * rounding towards zero.
     *
     * @return The number of milliseconds.
     */
    public long toMillis() {
        return unit.toMillis(length);
    }

    /**
     * Return the length of this timeout measured in whole seconds, rounding
     * towards zero.
     *
     * @return The number of seconds.
     */
    public long toSeconds() {
        return unit.toSeconds(length);
    }

    /**
     * Return the length of this timeout measured in whole minutes, rounding
     * towards zero.
     *
     * @return The number of minutes.
     */
    public long toMinutes() {
        return unit.toMinutes(length);
    }

    /**
     * Return the length of this timeout measured in whole hours, rounding
     * towards zero.
     *
     * @return The number of hours.
     */
    public long toHours() {
        return unit.toHours(length);
    }

    /**
     * Return the length of this timeout measured in whole days, rounding
     * towards zero.
     *
     * @return The number of days.
     */
    public long toDays() {
        return unit.toDays(length);
    }

    /**
     * Return the number of nanoseconds as floating point number, scaled down to
     * the given unit. The result may not precisely represent this timeout due
     * to the Double datatype's inherent limitations (mantissa size effectively
     * 53 bits).
     *
     * @param u The unit to scale to.
     * @return The length scaled to the given unit.
     */
    public double toUnit(TimeUnit u) {
        return (double) toNanos() / NANOSECONDS.convert(1, u);
    }

    private String unitString() {
        return timeUnitName.get(unit) + (length == 1 ? "" : "s");
    }

    @Override
    public String toString() {
        return "" + length + " " + unitString();
    }

    @Override
    public int compareTo(Timeout other) {
        return Long.compare(toNanos(), other.toNanos());
    }

    /**
     * Return timeout which is equal to this duration but with a coarsest Unit,
     * or self in case it is already the coarsest Unit
     * <p>
     * Examples:<br>
     * <pre>
     * Timeout.create(60, MINUTES).toCoarsest // Timeout.create(1, HOURS)
     * Timeout.create(1000, MILLISECONDS).toCoarsest // Timeout.create(1, SECONDS)
     * Timeout.create(48, HOURS).toCoarsest // Timeout.create(2, DAYS)
     * Timeout.create(5, SECONDS).toCoarsest // Timeout.create(5, SECONDS)
     * </pre>
     *
     * @return The coarsest timeout object.
     */
    public Timeout toCoarsest() {
        if (unit == DAYS || length == 0) {
            return this;
        } else {
            return loop(length, unit);
        }
    }

    private Timeout loop(long length, TimeUnit unit) {
        switch (unit) {
            case DAYS:
                return new Timeout(length, unit);
            case HOURS:
                return coarserOrThis(length, unit, DAYS, 24);
            case MINUTES:
                return coarserOrThis(length, unit, HOURS, 60);
            case SECONDS:
                return coarserOrThis(length, unit, MINUTES, 60);
            case MILLISECONDS:
                return coarserOrThis(length, unit, SECONDS, 1000);
            case MICROSECONDS:
                return coarserOrThis(length, unit, MILLISECONDS, 1000);
            case NANOSECONDS:
                return coarserOrThis(length, unit, MICROSECONDS, 1000);
            default:
                throw new IllegalArgumentException();
        }
    }

    private Timeout coarserOrThis(long length, TimeUnit unit, TimeUnit coarser, int divider) {
        if (length % divider == 0) {
            return loop(length / divider, coarser);
        } else if (unit == this.unit) {
            return this;
        } else {
            return new Timeout(length, unit);
        }
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Timeout) {
            return toNanos() == ((Timeout) other).toNanos();
        }
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        return (int) toNanos();
    }
}
