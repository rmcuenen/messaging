package cuenen.raymond.messaging.transport;

import cuenen.raymond.messaging.DataMessage;
import cuenen.raymond.messaging.MessageConverter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class JavaSerializer implements MessageConverter {

    @Override
    public DataMessage decode(String messageType, byte[] message) throws Exception {
        final ByteArrayInputStream in = new ByteArrayInputStream(message);
        final Serializable object;
        try (ObjectInputStream stream = new ObjectInputStream(in)) {
            object = (Serializable) stream.readObject();
        }
        return (object instanceof DataMessage) ? (DataMessage) object : JavaMessage.wrap(object);
    }

    @Override
    public byte[] encode(DataMessage message) throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (ObjectOutputStream stream = new ObjectOutputStream(out)) {
            if (message instanceof JavaMessage) {
                stream.writeObject(JavaMessage.unwrap((JavaMessage) message));
            } else if (message instanceof Serializable) {
                stream.writeObject(message);
            } else {
                throw new IllegalArgumentException("Unsupported message type: " + message.getClass());
            }
            return out.toByteArray();
        }
    }
}
