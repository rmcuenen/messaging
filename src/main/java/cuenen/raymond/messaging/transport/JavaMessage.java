package cuenen.raymond.messaging.transport;

import cuenen.raymond.messaging.AbstractDataMessage;
import java.io.Serializable;

/**
 *
 */
public final class JavaMessage extends AbstractDataMessage {

    public static JavaMessage wrap(Serializable object) {
        return new JavaMessage(object);
    }

    public static Serializable unwrap(JavaMessage message) {
        return message.object;
    }

    private final Serializable object;

    private JavaMessage(Serializable object) {
        this.object = object;
    }

    @Override
    public String getMessageType() {
        return object.getClass().getName();
    }

    @Override
    public int hashCode() {
        return 41 + object.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JavaMessage) {
            JavaMessage that = (JavaMessage) obj;
            return this.object.equals(that.object);
        }
        return false;
    }

    @Override
    public String toString() {
        return object.toString();
    }
}
