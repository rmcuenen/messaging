package cuenen.raymond.messaging;

/**
 * The argument {@code x-match} is understood by the {@link MessageSelector}. It
 * can take two values, dictating how the name value pairs of the
 * {@link MessageSelector} are treated during matching.
 */
public enum XMatch {

    /**
     * {@code All} implies that all the fields in the {@link MessageSelector}
     * table must match the header properties of a message for that message to
     * be accepted (i&#46;e&#46; an AND match).
     */
    All,
    /**
     * {@code Any} implies that the message should be accepted if any of the
     * fields in the header properties match one of the fields in the
     * {@link MessageSelector} table (i&#46;e&#46; an OR match).
     */
    Any;

    /**
     * Binding argument name for the matcher.
     */
    public static final String Argument = "x-match";

    /**
     * Returns the value for the {@link #Argument} property.
     *
     * @return The {@code x-match} value.
     */
    public String value() {
        return name().toLowerCase();
    }

    /**
     * Returns the logical operator of this matcher.
     *
     * @return The logical operator.
     */
    public String operator() {
        switch (this) {
            case All:
                return "AND";
            case Any:
                return "OR";
            default:
                throw new IllegalArgumentException("Unknown matcher");
        }
    }
}
