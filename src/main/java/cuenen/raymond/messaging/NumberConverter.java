package cuenen.raymond.messaging;

import java.lang.reflect.Constructor;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Converter class to convert numbers form their string representation into
 * their number type.
 */
public final class NumberConverter {

    private static final List<Class<? extends Number>> IntegerTypes = new ArrayList<>();
    private static final List<Class<? extends Number>> DoubleTypes = new ArrayList<>();

    static {
        IntegerTypes.add(Byte.class);
        IntegerTypes.add(Short.class);
        IntegerTypes.add(Integer.class);
        IntegerTypes.add(Long.class);
        IntegerTypes.add(BigInteger.class);
        DoubleTypes.add(Float.class);
        DoubleTypes.add(Double.class);
        DoubleTypes.add(BigDecimal.class);
    }

    /**
     * Convert the given string representation into a {@code Number}.
     *
     * @param rawValue The string representation.
     * @return The typed number.
     * @throws NumberFormatException If the string does not contain a parsable
     * number.
     */
    public static Number valueOf(String rawValue) {
        Number value = convertToIntegerType(rawValue);
        if (value == null) {
            value = convertToDoubleType(rawValue);
        }
        if (value == null) {
            throw new NumberFormatException(rawValue + " is not a number");
        }
        return value;
    }

    private static Number convertToIntegerType(String value) {
        for (Class<? extends Number> typeClass : IntegerTypes) {
            try {
                Constructor<? extends Number> constructor = typeClass.getConstructor(String.class);
                return constructor.newInstance(value);
            } catch (Exception ex) {
                // Ignore
            }
        }
        return null;
    }

    private static Number convertToDoubleType(String value) {
        for (Class<? extends Number> typeClass : DoubleTypes) {
            try {
                Constructor<? extends Number> constructor = typeClass.getConstructor(String.class);
                return constructor.newInstance(value);
            } catch (Exception ex) {
                // Ignore
            }
        }
        return null;
    }

    private NumberConverter() {
        // Utility class
    }
}
