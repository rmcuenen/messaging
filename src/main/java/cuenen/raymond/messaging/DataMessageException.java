package cuenen.raymond.messaging;

/**
 * This is the root class of all checked exceptions in the DataMessage API. It
 * provides a string describing the error. This string is the standard exception
 * message and is available via the {@code getMessage} method.
 */
public class DataMessageException extends Exception {

    /**
     * Constructs a {@link DataMessageException} with the specified reason.
     *
     * @param reason A description of the exception.
     */
    public DataMessageException(String reason) {
        super(reason);
    }
}
