package cuenen.raymond.messaging;

/**
 * This exception is thrown when an attempt is made to set properties on a
 * read-only message.
 */
public class MessageNotWriteableException extends DataMessageException {

    /**
     * Constructs a {@link MessageNotWriteableException} with the specified
     * reason.
     *
     * @param reason A description of the exception.
     */
    public MessageNotWriteableException(String reason) {
        super(reason);
    }
}
