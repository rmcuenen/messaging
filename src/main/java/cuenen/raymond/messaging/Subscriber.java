package cuenen.raymond.messaging;

import java.util.Collections;
import java.util.Random;
import java.util.function.Consumer;

/**
 * This class encapsulates the information needed to receive messages through a
 * publish-subscribe pattern using message queues.
 */
public final class Subscriber {

    /**
     * Creates a new {@link Subscriber} object, using a random subscriber
     * identifier and no message selector.
     *
     * @param topicName The topic name to subscribe to.
     * @param callback The callback function for receiving messages.
     * @return The subscriber.
     */
    public static Subscriber create(String topicName, Consumer<? extends DataMessage> callback) {
        return new Subscriber(topicName, createSubscriberId(), Default, callback);
    }

    /**
     * Creates a new {@link Subscriber} object with no message selector.
     *
     * @param topicName The topic name to subscribe to.
     * @param subscriberId The subscriber identifier for the subscription.
     * @param callback The callback function for receiving messages.
     * @return The subscriber.
     */
    public static Subscriber create(String topicName, String subscriberId,
            Consumer<? extends DataMessage> callback) {
        return new Subscriber(topicName, subscriberId, Default, callback);
    }

    /**
     * Creates a new {@link Subscriber} object, using a random subscriber
     * identifier and the provided message selector.
     *
     * @param topicName The topic name to subscribe to.
     * @param selector The message selector to use.
     * @param callback The callback function for receiving messages.
     * @return The subscriber.
     */
    public static Subscriber create(String topicName, MessageSelector selector,
            Consumer<? extends DataMessage> callback) {
        return new Subscriber(topicName, createSubscriberId(), selector, callback);
    }

    /**
     * Creates a new {@link Subscriber} object.
     *
     * @param topicName The topic name to subscribe to.
     * @param subscriberId The subscriber identifier for the subscription.
     * @param selector The message selector to use.
     * @param callback The callback function for receiving messages.
     * @return The subscriber.
     */
    public static Subscriber create(String topicName, String subscriberId,
            MessageSelector selector, Consumer<? extends DataMessage> callback) {
        return new Subscriber(topicName, subscriberId, selector, callback);
    }

    /**
     * Creates a random subscriber identifier.
     *
     * @return A random subscriber identifier.
     */
    private static String createSubscriberId() {
        final Random random = new Random(System.currentTimeMillis());
        final int randomInt = random.nextInt();
        return Long.toHexString(randomInt & 0x00000000ffffffffL);
    }

    /**
     * The default {@link MessageSelector}. This matches all the messages, e.g.
     * no filtering.
     */
    public static final MessageSelector Default = new MessageSelector(XMatch.All, Collections.<HeaderProperty>emptySet());

    private final String topicName;
    private final String subscriberId;
    private final MessageSelector selector;
    private final Consumer<? extends DataMessage> callback;

    /**
     * Creates a new {@link Subscriber} object.
     *
     * @param topicName The topic name to subscribe to.
     * @param subscriberId The subscriber identifier for the subscription.
     * @param selector The message selector to use (optional).
     * @param callback The callback function for receiving messages.
     */
    public Subscriber(String topicName, String subscriberId,
            MessageSelector selector, Consumer<? extends DataMessage> callback) {
        this.topicName = topicName;
        this.subscriberId = subscriberId;
        this.selector = selector;
        this.callback = callback;
    }

    /**
     * Returns the topic name to subscribe to.
     *
     * @return The topic name.
     */
    public String getTopicName() {
        return topicName;
    }

    /**
     * Returns the subscriber identifier for the subscription.
     *
     * @return The subscriber identifier.
     */
    public String getSubscriberId() {
        return subscriberId;
    }

    /**
     * Returns the message selector to use. The default value is a message
     * selector that matches all messages.
     *
     * @return The message selector.
     * @see #Default Default
     */
    public MessageSelector getMessageSelector() {
        return selector;
    }

    /**
     * Returns the callback function for receiving messages.
     *
     * @return The callback function.
     */
    public Consumer<? extends DataMessage> getCallback() {
        return callback;
    }

    @Override
    public int hashCode() {
        return 41 * (41 * (41 * (41 + topicName.hashCode()) + subscriberId.hashCode())
                + selector.hashCode()) + callback.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Subscriber) {
            Subscriber that = (Subscriber) obj;
            return this.topicName.equals(that.topicName)
                    && this.subscriberId.equals(that.subscriberId)
                    && this.selector.equals(that.selector)
                    && this.callback.equals(that.callback);
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("Subscriber(%s, %s, %s, %s)", topicName, subscriberId,
                selector, callback);
    }
}
