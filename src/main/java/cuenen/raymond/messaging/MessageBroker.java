package cuenen.raymond.messaging;

import cuenen.raymond.messaging.client.ActiveMQClient;
import cuenen.raymond.messaging.client.FFMQClient;
import cuenen.raymond.messaging.client.OpenMQClient;
import cuenen.raymond.messaging.client.RabbitMQClient;

/**
 * Enumeration used to define the message broker client implementation to
 * instantiate.
 */
public enum MessageBroker {

    /**
     * Represents the ActiveMQ Client implementation.
     */
    ActiveMQ {
                @Override
                public Client client(MessageConverter messageConverter, BrokerProperties properties) {
                    return ActiveMQClient.create(messageConverter, properties);
                }
            },
    /**
     * Represents the FFMQ Client implementation.
     */
    FFMQ {
                @Override
                public Client client(MessageConverter messageConverter, BrokerProperties properties) {
                    return FFMQClient.create(messageConverter, properties);
                }
            },
    /**
     * Represents the OpenMQ Client implementation.
     */
    OpenMQ {
                @Override
                public Client client(MessageConverter messageConverter, BrokerProperties properties) {
                    return OpenMQClient.create(messageConverter, properties);
                }
            },
    /**
     * Represents the RabbitMQ Client implementation.
     */
    RabbitMQ {
                @Override
                public Client client(MessageConverter messageConverter, BrokerProperties properties) {
                    return RabbitMQClient.create(messageConverter, properties);
                }
            };

    /**
     * Create the client based on the given parameters.
     *
     * @param messageConverter The message converter.
     * @param properties The broker properties.
     * @return The client implementation.
     */
    public abstract Client client(MessageConverter messageConverter, BrokerProperties properties);

    /**
     * Convenience method to extract the deepest exception message. The result
     * is the first non-null message of the exception hierarchy.
     *
     * @param t The top exception to get the message out.
     * @return The exception message.
     */
    public static String getMessage(Throwable t) {
        String msg = t.getMessage();
        while (t.getCause() != null) {
            t = t.getCause();
            msg = t.getMessage() == null ? msg : t.getMessage();
        }
        return msg;
    }
}
