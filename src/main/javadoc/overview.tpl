<html>
    <body>
        Messaging.
        <h1>Using the Messaging Library</h1>
        <p>
            The Message Client distribution contains Client implementations
            that can aid in the development of applications. A Client provides
            an abstraction to the underlying message broker used to communicate
            with another application or server.
        </p>
        <h2>Messages</h2>
        <p>
            TODO
        </p>
        <h3>Properties</h3>
        <p>
            TODO
        </p>
        <h3>Converter</h3>
        <p>
            TODO
        </p>
        <h2>Client</h2>
        <p>
            Obtaining a {@link cuenen.raymond.messaging.Client} is
            simple. The most common way to get a client is by:
        </p>
        <ol>
            <li>
                Creating a {@link cuenen.raymond.messaging.Client.ClientBuilder ClientBuilder}
                that allows for configuration of the client.
            </li>
            <li>
                Requesting the construction of the
                {@link cuenen.raymond.messaging.Client} from the builder.
            </li>
        </ol>
        <p>
            Another manner is by instantiating one of the implemented Client
            classes directly.
        </p>
        <dl>
            <dt><strong>Important:</strong></dt>
            <dd>
                Please note that the actual client dependencies are not
                included in the library. Depending on the message broker, you
                need to have the correct dependencies on your classpath in order
                to connect to that message broker.
            </dd>
            <dd>
                This release of the Message Client contains implementations for
                use with the message brokers <a href="http://activemq.apache.org/" target="_blank">ActiveMQ</a>
                (v@ACTIVEMQ@), <a href="http://timewalker74.github.io/ffmq/" target="_blank">FFMQ</a>
                (v@FFMQ@), <a href="https://mq.java.net/" target="_blank">OpenMQ</a>
                (v@OPENMQ@) and <a href="http://www.rabbitmq.com/" target="_blank">RabbitMQ</a>
                (v@RABBITMQ@).
            </dd>
        </dl>
        <p>
            Creating a {@link cuenen.raymond.messaging.Client} with a
            connection to ActiveMQ for example can be as simple as:
        </p>
        <pre style="background:#f9f9f9;color:#080808"><span style="color:#794938">import</span> <span style="color:#a71d5d;font-style:italic">cuenen.raymond.messaging.Client</span>;
<span style="color:#794938">import</span> <span style="color:#a71d5d;font-style:italic">cuenen.raymond.messaging.MessageBroker</span>;

<span style="color:#5a525f;font-style:italic">// on startup</span>

<span style="color:#a71d5d;font-style:italic">Client</span> client <span style="color:#794938">=</span> clientBuilder()<span style="color:#794938">.</span>broker(<span style="color:#a71d5d;font-style:italic">MessageBroker</span><span style="color:#794938">.</span><span style="color:#a71d5d;font-style:italic">ActiveMQ</span>)<span style="color:#794938">.</span>build();
client<span style="color:#794938">.</span>connect();

<span style="color:#5a525f;font-style:italic">// on shutdown</span>

client<span style="color:#794938">.</span>close();
        </pre>
        <p>
            This will create a {@link cuenen.raymond.messaging.client.JMSClient}
            that connects to the default ActiveMQ broker URL using the default
            username and password. The creation of a client connecting to another
            broker is done in the same way. Just change the
            {@link cuenen.raymond.messaging.MessageBroker} value.
        </p>
        <p>
            To change any of the client connection settings, invoke the
            appropriate helper method on the builder. Other properties can also
            be set when needed. Refer to the appropriate {@link cuenen.raymond.messaging.Client}
            implementation for more details on supported properties.
        </p>
        <table style="width: 100%; text-align: left; border-collapse: collapse;">
            <tbody>
                <tr>
                    <th style="border-bottom: 2px solid #969696;"><strong>Parameter</strong></th>
                    <th style="border-bottom: 2px solid #969696;"><strong>Description</strong></th>
                </tr>
            </tbody>
            <tbody>
                <tr>
                    <td style="border-bottom: 1px solid #969696;">{@code converter}</td>
                    <td style="border-bottom: 1px solid #969696;">Sets the message converter to use for the client.</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #969696;">{@code connection}</td>
                    <td style="border-bottom: 1px solid #969696;">Sets the connection URL used to connect to the message broker.</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #969696;">{@code user}</td>
                    <td style="border-bottom: 1px solid #969696;">Sets the user name to use when connecting to the message broker.</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #969696;">{@code password}</td>
                    <td style="border-bottom: 1px solid #969696;">Sets the password to use when connecting to the message broker.</td>
                </tr>
            </tbody>
        </table>
        <h3>RabbitMQ specific properties</h3>
        <p>
            The {@link cuenen.raymond.messaging.client.RabbitMQClient} implementation
            needs to declare an exchange to bind to. The declare parameters used
            during exchange declare can be adjusted for a client instance.
        </p>
        <table  style="width: 100%; text-align: left; border-collapse: collapse;">
            <tbody>
                <tr>
                    <th style="border-bottom: 2px solid #969696;"><strong>Parameter</strong></th>
                    <th style="border-bottom: 2px solid #969696;"><strong>Default</strong></th>
                    <th style="border-bottom: 2px solid #969696;"><strong>Possible values</strong></th>
                </tr>
            </tbody>
            <tbody>
                <tr>
                    <td style="border-bottom: 1px solid #969696;">{@code exchangeType}</td>
                    <td style="border-bottom: 1px solid #969696;">{@code headers}</td>
                    <td style="border-bottom: 1px solid #969696;">{@code direct}, {@code fanout}, {@code topic} or {@code headers}.</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #969696;">{@code durable}</td>
                    <td style="border-bottom: 1px solid #969696;">{@code true}</td>
                    <td style="border-bottom: 1px solid #969696;">{@code true} or {@code false}.</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #969696;">{@code autoDelete}</td>
                    <td style="border-bottom: 1px solid #969696;">{@code false}</td>
                    <td style="border-bottom: 1px solid #969696;">{@code true} or {@code false}.</td>
                </tr>
            </tbody>
        </table>
        <h2>Publish</h2>
        <p>
            TODO
        </p>
        <h2>Subscription</h2>
        <p>
            The Message Client allows one to take a subscription for messages
            published on a topic. This is done by registering a
            {@link java.util.function.Consumer} under an unique subscriber identifier
            with the topic name where the messages will be received from.
            Here is an example:
        </p>
        <pre style="background:#f9f9f9;color:#080808"><span style="color:#794938">import</span> <span style="color:#a71d5d;font-style:italic">cuenen.raymond.messaging.MessageData</span>;
<span style="color:#794938">import</span> <span style="color:#a71d5d;font-style:italic">cuenen.raymond.messaging.Subscriber</span>;

<span style="color:#a71d5d;font-style:italic">Subscriber</span> subscriber <span style="color:#794938">=</span> <span style="color:#a71d5d;font-style:italic">Subscriber</span><span style="color:#794938">.</span>create(<span style="color:#0b6125">"BS_MSG"</span>, <span style="color:#0b6125">"msg-subscriber"</span>, message <span style="color:#794938">-</span><span style="color:#794938">></span> <span style="color:#a71d5d;font-style:italic">System</span><span style="color:#794938">.</span>out<span style="color:#794938">:</span><span style="color:#794938">:</span>println);
client<span style="color:#794938">.</span>subscribe(subscriber);
        </pre>
        <p>
            This will instruct the message broker to subscribe to the topic
            {@code "BS_MSG"} and deliver all received messages to the given
            {@link java.util.function.Consumer}. This function simply prints the received
            messages to standard out.
        </p>
        <p>
            The subscriber is identified by the string {@code "msg-subscriber"}.
            Note that the combination topic name and subscriber identifier must
            be unique. To unsubscribe the previous registered subscriber you
            can write:
        </p>
        <pre style="background:#f9f9f9;color:#080808">
client<span style="color:#794938">.</span>unsubscribe(subscriber);
        </pre>
        <h3>Message Selector</h3>
        <p>
            A message selector allows a client to specify, by header field
            references and property references, the messages it is interested
            in. Only messages whose header and property values match the
            selector are delivered. Message selectors cannot reference message
            body values.
        </p>
        <p>
            A message selector matches a message if the selector evaluates to
            true when the message’s header field values and property values are
            substituted for their corresponding identifiers in the selector. If
            a field in the selector has no value the message is matched if there
            is a corresponding message header field with any value.
        </p>
        <p>
            There are two types of message selectors;
        </p>
        <ol>
            <li>
                {@link cuenen.raymond.messaging.XMatch#All All}
                implies that all the fields in the message selector must
                match the header properties of a message (i.e. an AND match).
            </li>
            <li>
                {@link cuenen.raymond.messaging.XMatch#All Any}
                implies that at least one of the fields in the message
                selector must match the header properties of a message
                (i.e. an OR match).
            </li>
        </ol>
        <p>
            A message selector is provided by the
            {@link cuenen.raymond.messaging.Subscriber} object during
            subscription and can be created with the
            {@link cuenen.raymond.messaging.MessageSelector.MessageSelectorBuilder MessageSelectorBuilder}.
            Here is an example:
        </p>
        <pre style="background:#f9f9f9;color:#080808"><span style="color:#794938">import</span> <span style="color:#a71d5d;font-style:italic">cuenen.raymond.messaging.Subscriber</span>;
<span style="color:#794938">import</span> <span style="color:#a71d5d;font-style:italic">cuenen.raymond.messaging.MessageSelector</span>;
<span style="color:#794938">import static</span> <span style="color:#a71d5d;font-style:italic">cuenen.raymond.messaging.MessageSelector.MessageSelectorBuilder.*</span>;
<span style="color:#794938">import</span> <span style="color:#a71d5d;font-style:italic">cuenen.raymond.messaging.XMatch</span>;

<span style="color:#a71d5d;font-style:italic">MessageSelector</span> selector <span style="color:#794938">=</span> messageSelector()
        .matcher(<span style="color:#a71d5d;font-style:italic">XMatch</span><span style="color:#794938">.</span><span style="color:#a71d5d;font-style:italic">All</span>)
        .setIntProperty(<span style="color:#0b6125">"DestinationPort"</span>, <span style="color:#811f24;font-weight:700">3002</span>)
        .setStringProperty(<span style="color:#0b6125">"Type"</span>, <span style="color:#0b6125">"BTP-Data-indication"</span>)
        .build();
<span style="color:#a71d5d;font-style:italic">Subscriber</span> subscriber <span style="color:#794938">=</span> <span style="color:#a71d5d;font-style:italic">Subscriber</span><span style="color:#794938">.</span>create(<span style="color:#0b6125">"TSM_NOTIFICATION"</span>, selector, callback);
client<span style="color:#794938">.</span>subscribe(subscriber);
        </pre>
        <p>
            This will instruct the message broker to subscribe to the topic
            {@code "TSM_NOTIFICATION"} and deliver only received messages that
            have the header property {@code "DestinationPort"} set to {@code 3002}
            AND the header property {@code "Type"} set to {@code "BTP-Data-indication"}
            to the given callback function.
        </p>
        <h2>Request</h2>
        <p>
            The Message Client allows one to perform an asynchronous
            request-response through the message broker. The general
            implementation of this pattern is for the client to create a
            temporary queue on which the response will be received. The address
            of this queue is sent in the header of the request message to the
            queue where the server is consuming for requests. Here is an
            example:
        </p>
        <pre style="background:#f9f9f9;color:#080808"><span style="color:#794938">import</span> <span style="color:#a71d5d;font-style:italic">cuenen.raymond.messaging.MessageData</span>;
<span style="color:#794938">import</span> <span style="color:#a71d5d;font-style:italic">cuenen.raymond.messaging.Request</span>;
<span style="color:#794938">import</span> <span style="color:#a71d5d;font-style:italic">cuenen.raymond.messaging.Timeout</span>;

<span style="color:#a71d5d;font-style:italic">final</span> <span style="color:#a71d5d;font-style:italic">MessageData</span> message <span style="color:#794938">=</span> createRequest();
<span style="color:#a71d5d;font-style:italic">final</span> <span style="color:#a71d5d;font-style:italic">Timeout</span> timeout <span style="color:#794938">=</span> <span style="color:#a71d5d;font-style:italic">Timeout</span><span style="color:#794938">.</span>create(<span style="color:#0b6125">"3 seconds"</span>);
<span style="color:#a71d5d;font-style:italic">final</span> <span style="color:#a71d5d;font-style:italic">Request</span> request <span style="color:#794938">=</span> <span style="color:#a71d5d;font-style:italic">Request</span><span style="color:#794938">.</span>create(<span style="color:#0b6125">"MSG_SEND"</span>, message, timeout);
client<span style="color:#794938">.</span>send(request)
        .whenComplete((response, error) <span style="color:#794938">-</span><span style="color:#794938">></span> {
            <span style="color:#a71d5d;font-style:italic">System</span><span style="color:#794938">.</span>out<span style="color:#794938">.</span>println(response);
            <span style="color:#a71d5d;font-style:italic">System</span><span style="color:#794938">.</span>out<span style="color:#794938">.</span>println(error);
        });
        </pre>
        <p>
            This will instruct the message broker to create a temporary
            reply-queue and send the request message to the queue
            {@code "MSG_SEND"}. When the response is received the returned
            {@link java.util.concurrent.CompletableFuture} is completed with
            the response message (or an exception) and the temporary queue is
            deleted.
        </p>
        <p>
            The given timeout of 3 seconds is the maximum time to wait for a
            reply from the server. If no reply is received within that time
            period, the returned {@link java.util.concurrent.CompletableFuture}
            is exceptionally completed with a {@link java.util.concurrent.TimeoutException}.
        </p>
        <p>
            Each message being send will get a header with the name
            {@link cuenen.raymond.messaging.Client#MessageTypeHeader MessageTypeHeader}
            and the value set to the type name of the message.
            Also the following message broker specific properties are set on
            the message before sending.
        </p>
        <table style="width: 100%; text-align: left; border-collapse: collapse;">
            <tbody>
                <tr>
                    <th style="border-bottom: 2px solid #969696;"><strong>JMS</strong></th>
                    <th style="border-bottom: 2px solid #969696;"><strong>AMQP</strong></th>
                    <th style="border-bottom: 2px solid #969696;"><strong>Description</strong></th>
                </tr>
            </tbody>
            <tbody>
                <tr>
                    <td style="border-bottom: 1px solid #969696;">{@code JMSCorrelationID}</td>
                    <td style="border-bottom: 1px solid #969696;">{@code correlation-id}</td>
                    <td style="border-bottom: 1px solid #969696;">An application correlation identifier.</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #969696;">{@code JMSReplayTo}</td>
                    <td style="border-bottom: 1px solid #969696;">{@code reply-to}</td>
                    <td style="border-bottom: 1px solid #969696;">The address to reply to.</td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #969696;">{@code JMSXUserID}<sup><a title="Only when it is enabled in the broker configuration by setting the populateJMSXUserID property.">1</a></sup></td>
                    <td style="border-bottom: 1px solid #969696;">{@code user-id}</td>
                    <td style="border-bottom: 1px solid #969696;">The authenticated username of the sender.</td>
                </tr>
            </tbody>
        </table>
        <h2>AtomicTime</h2>
        <p>
            International Atomic Time (TAI) is a high-precision atomic
            coordinate time standard based on the notional passage of proper
            time on Earth's geoid. It is the basis for Coordinated Universal
            Time (UTC), which is used for civil timekeeping all over the Earth's
            surface, and for Terrestrial Time, which is used for astronomical
            calculations
            <sup><a href="http://en.wikipedia.org/wiki/International_Atomic_Time" title="From Wikipedia, the free encyclopedia">[1]</a></sup>.
        </p>
        <p>
            While most clocks derive their time from Coordinated Universal Time
            (UTC), the atomic clocks on the satellites are set to GPS time. The
            difference is that GPS time is not corrected to match the rotation
            of the Earth, so it does not contain leap seconds or other
            corrections that are periodically added to UTC. GPS time was set to
            match UTC in 1980, but has since diverged. The lack of corrections
            means that GPS time remains at a constant offset with International
            Atomic Time (TAI) (TAI − GPS = 19 seconds)
            <sup><a href="http://en.wikipedia.org/wiki/Global_Positioning_System#Timekeeping" title="From Wikipedia, the free encyclopedia">[2]</a></sup>.
        </p>
        <p>
            To convert between milliseconds in UTC, milliseconds in TAI and
            milliseconds in GPS time an utility class,
            {@link cuenen.raymond.messaging.AtomicTime}, is created.
        </p>
    </body>
</html>