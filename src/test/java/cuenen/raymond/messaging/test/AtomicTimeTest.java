package cuenen.raymond.messaging.test;

import cuenen.raymond.messaging.AtomicTime;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

public class AtomicTimeTest {

    private static final long Epoch = 0L;
    private static final long UTC_EPOCH_IN_TAI = 10000L;
    private static final long TAI_EPOCH_IN_GPS = -19000L;
    private static final long UTC_EPOCH_IN_GPS = UTC_EPOCH_IN_TAI + TAI_EPOCH_IN_GPS;

    @BeforeClass
    public static void setUpClass() {
        DateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        parser.setTimeZone(TimeZone.getTimeZone("UTC"));
        final long now = System.currentTimeMillis();
        System.out.println("UTC: " + parser.format(new Date(now)));
        System.out.println("GPS: " + parser.format(new Date(AtomicTime.utcToGPS(now))));
        System.out.println("TAI: " + parser.format(new Date(AtomicTime.utcToTAI(now))));
    }

    /**
     * Test of utcToTAI method, of class AtomicTime.
     */
    @Test
    public void testUtcToTAI() {
        assertEquals(UTC_EPOCH_IN_TAI, AtomicTime.utcToTAI(Epoch));
    }

    /**
     * Test of utcToGPS method, of class AtomicTime.
     */
    @Test
    public void testUtcToGPS() {
        assertEquals(UTC_EPOCH_IN_GPS, AtomicTime.utcToGPS(Epoch));
    }

    /**
     * Test of taiToUTC method, of class AtomicTime.
     */
    @Test
    public void testTaiToUTC() {
        assertEquals(-UTC_EPOCH_IN_TAI, AtomicTime.taiToUTC(Epoch));
    }

    /**
     * Test of taiToGPS method, of class AtomicTime.
     */
    @Test
    public void testTaiToGPS() {
        assertEquals(TAI_EPOCH_IN_GPS, AtomicTime.taiToGPS(Epoch));
    }

    /**
     * Test of gpsToUTC method, of class AtomicTime.
     */
    @Test
    public void testGpsToUTC() {
        assertEquals(-UTC_EPOCH_IN_GPS, AtomicTime.gpsToUTC(Epoch));
    }

    /**
     * Test of gpsToTAI method, of class AtomicTime.
     */
    @Test
    public void testGpsToTAI() {
        assertEquals(-TAI_EPOCH_IN_GPS, AtomicTime.gpsToTAI(Epoch));
    }

}
