package cuenen.raymond.messaging.test;

import com.sun.messaging.ConnectionConfiguration;
import com.sun.messaging.ConnectionFactory;
import com.sun.messaging.jmq.jmsclient.runtime.BrokerInstance;
import com.sun.messaging.jmq.jmsclient.runtime.ClientRuntime;
import com.sun.messaging.jmq.jmsserver.Globals;
import com.sun.messaging.jmq.jmsservice.BrokerEvent;
import com.sun.messaging.jmq.jmsservice.BrokerEventListener;
import cuenen.raymond.messaging.Client;
import static cuenen.raymond.messaging.Client.ClientBuilder.clientBuilder;
import cuenen.raymond.messaging.MessageBroker;
import static cuenen.raymond.messaging.test.JMSBroker.findFreePort;
import java.util.Properties;

public class OpenMQClientTest extends ClientTest {

    private static final int BrokerPort = findFreePort(7676);
    private static final String BrokerURL = "mq://localhost:" + BrokerPort;

    private static class OpenMQBroker extends JMSBroker {

        private static final String IMQ_HOME = OpenMQBroker.class.getClassLoader().getResource("openmq").getPath();
        private static final String IMQ_VAR_HOME = IMQ_HOME.concat("/var");
        private static final String IMQ_LIB_HOME = IMQ_HOME.concat("/lib");
        private static final String IMQ_INSTANCE_HOME = IMQ_VAR_HOME.concat("/instances");
        private static final String IMQ_INSTANCE_NAME = "imqbroker";
        private final BrokerInstance brokerInstance;

        public OpenMQBroker() {
            super(new ConnectionFactory());
            BrokerInstance broker = null;
            try {
                ((ConnectionFactory) connectionFactory).setProperty(ConnectionConfiguration.imqAddressList, BrokerURL);
                ClientRuntime clientRuntime = ClientRuntime.getRuntime();
                broker = clientRuntime.createBrokerInstance();
                BrokerEventListener listener = new BrokerEventListener() {

                    @Override
                    public void brokerEvent(BrokerEvent event) {
                    }

                    @Override
                    public boolean exitRequested(BrokerEvent event, Throwable thr) {
                        return true;
                    }
                };

                Properties properties = broker.parseArgs(new String[]{
                    "-varhome", IMQ_VAR_HOME,
                    "-libhome", IMQ_LIB_HOME,
                    "-imqhome", IMQ_HOME
                });
                Globals.getConfig().updateProperty("imq.home", IMQ_HOME);
                Globals.getConfig().updateProperty("imq.libhome", IMQ_LIB_HOME);
                Globals.getConfig().updateProperty("imq.varhome", IMQ_VAR_HOME);
                Globals.getConfig().updateProperty("imq.instanceshome", IMQ_INSTANCE_HOME);
                Globals.getConfig().updateProperty("imq.instancename", IMQ_INSTANCE_NAME);
                Globals.getConfig().updateBooleanProperty("imq.persist.file.newTxnLog.enabled", false, true);
                Globals.getConfig().updateBooleanProperty("imq.cluster.enabled", false, true);
                Globals.getConfig().updateIntProperty("imq.portmapper.port", BrokerPort, true);
                broker.init(properties, listener);
            } catch (Exception ex) {
                // Ignore
            }
            brokerInstance = broker;
        }

        @Override
        public void startBroker() throws Exception {
            brokerInstance.start();
        }

        @Override
        public void stopBroker() throws Exception {
            brokerInstance.shutdown();
        }

        @Override
        protected boolean populateJMSXUserIDSupported() {
            return true;
        }

        private int index = 0;

        @Override
        public int getCurrentConnections() {
            //TODO How?
            return new int[]{0, 1, 1, 0}[index++];
        }
    }

    public OpenMQClientTest() {
        super(new OpenMQBroker());
    }

    @Override
    public Client createClient(String username, String password) {
        return clientBuilder()
                .broker(MessageBroker.OpenMQ)
                .converter(MessageConverter)
                .connection(BrokerURL)
                .user(username)
                .password(password)
                .build();
    }
}
