package cuenen.raymond.messaging.test;

import cuenen.raymond.messaging.Client;
import cuenen.raymond.messaging.DataMessage;
import static cuenen.raymond.messaging.test.ClientTest.*;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.Enumeration;
import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import static org.junit.Assert.*;

/**
 * Implementation of the common part of the Test broker for testing JMS.
 */
public abstract class JMSBroker implements Broker {

    public static final String Admin = "admin";

    public static int findFreePort(int defaultPort) {
        try (ServerSocket socket = new ServerSocket(0)) {
            socket.setReuseAddress(true);
            return socket.getLocalPort();
        } catch (IOException ex) {
            return defaultPort;
        }
    }

    protected final ConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;
    private MessageConsumer consumer;
    private MessageProducer producer;
    private MessageProducer replyProducer;

    public JMSBroker(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    protected abstract void startBroker() throws Exception;

    protected abstract void stopBroker() throws Exception;

    protected abstract boolean populateJMSXUserIDSupported();

    @Override
    public void start() throws Exception {
        startBroker();
        connection = connectionFactory.createConnection(Admin, Admin);
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Destination topic = session.createTopic(TopicName);
        producer = session.createProducer(topic);
        final Destination adminQueue = session.createQueue(QueueName);
        replyProducer = session.createProducer(null);
        replyProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        consumer = session.createConsumer(adminQueue);
    }

    @Override
    public void stop() throws Exception {
        if (connection != null) {
            connection.close();
        }
        stopBroker();
    }

    @Override
    public void publish(DataMessage message) throws Exception {
        final BytesMessage msg = session.createBytesMessage();
        for (Enumeration<String> names = message.getPropertyNames(); names.hasMoreElements();) {
            final String name = names.nextElement();
            msg.setObjectProperty(name, message.getObjectProperty(name));
        }
        msg.setStringProperty(Client.MessageTypeHeader, message.getMessageType());
        msg.writeBytes(MessageConverter.encode(message));
        producer.send(msg);
        Thread.sleep(100L);
    }

    @Override
    public void expectMessage(DataMessage message, DataMessage reply) throws Exception {
        final Message request = consumer.receive();
        assertEquals("Missing message type in header", message.getMessageType(), request.getStringProperty(Client.MessageTypeHeader));
        if (populateJMSXUserIDSupported()) {
            assertEquals("Missing username in header", Username, request.getStringProperty("JMSXUserID"));
        }
        if (request instanceof BytesMessage) {
            final BytesMessage msg = (BytesMessage) request;
            final byte[] buf = new byte[(int) msg.getBodyLength()];
            msg.readBytes(buf);
            assertArrayEquals(MessageConverter.encode(message), buf);
        }
        if (reply != null) {
            final BytesMessage msg = session.createBytesMessage();
            msg.setStringProperty(Client.MessageTypeHeader, reply.getMessageType());
            msg.setJMSCorrelationID(request.getJMSCorrelationID());
            msg.writeBytes(MessageConverter.encode(reply));
            replyProducer.send(request.getJMSReplyTo(), msg);
        }
        Thread.sleep(100L);
    }
}
