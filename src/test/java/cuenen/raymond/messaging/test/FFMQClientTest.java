package cuenen.raymond.messaging.test;

import cuenen.raymond.messaging.Client;
import static cuenen.raymond.messaging.Client.ClientBuilder.clientBuilder;
import cuenen.raymond.messaging.MessageBroker;
import static cuenen.raymond.messaging.test.JMSBroker.findFreePort;
import java.io.File;
import net.timewalker.ffmq3.FFMQCoreSettings;
import net.timewalker.ffmq3.FFMQServerSettings;
import net.timewalker.ffmq3.jndi.FFMQConnectionFactory;
import net.timewalker.ffmq3.listeners.ClientListener;
import net.timewalker.ffmq3.listeners.tcp.io.TcpListener;
import net.timewalker.ffmq3.local.FFMQEngine;
import net.timewalker.ffmq3.management.TemplateMapping;
import net.timewalker.ffmq3.management.destination.definition.QueueDefinition;
import net.timewalker.ffmq3.management.destination.definition.TopicDefinition;
import net.timewalker.ffmq3.management.destination.template.QueueTemplate;
import net.timewalker.ffmq3.utils.Settings;

/**
 * Test the implementation of the FFMQ client.
 */
public class FFMQClientTest extends ClientTest {

    private static final int BrokerPort = findFreePort(10002);
    private static final String BrokerURL = "tcp://localhost:" + BrokerPort;

    private static class FFMQBroker extends JMSBroker {

        private final FFMQEngine engine;
        private final ClientListener tcpListener;

        public FFMQBroker() {
            super(new FFMQConnectionFactory());
            try {
                final Settings settings = createSettings();
                engine = new FFMQEngine("ffmqengine", settings);
                tcpListener = new TcpListener(engine, "0.0.0.0", BrokerPort, settings, null);
                ((FFMQConnectionFactory) connectionFactory).setProviderURL("vm://ffmqengine");
                QueueTemplate template = new QueueTemplate();
                template.setName("TempQueueTemplate");
                template.setMaxNonPersistentMessages(10);
                template.setTemporary(true);
                engine.getDestinationTemplateProvider().addQueueTemplate(template);
                engine.getTemplateMappingProvider().addQueueTemplateMapping(new TemplateMapping("TEMP-QUEUE-*", "TempQueueTemplate"));
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }

        private Settings createSettings() {
            Settings settings = new Settings();
            settings.setStringProperty(FFMQCoreSettings.DESTINATION_DEFINITIONS_DIR, FFMQClientTest.class.getResource("/ffmq").getPath());
            settings.setStringProperty(FFMQCoreSettings.BRIDGE_DEFINITIONS_DIR, FFMQClientTest.class.getResource("/ffmq").getPath());
            settings.setStringProperty(FFMQCoreSettings.TEMPLATES_DIR, FFMQClientTest.class.getResource("/ffmq").getPath());
            settings.setStringProperty(FFMQCoreSettings.DEFAULT_DATA_DIR, FFMQClientTest.class.getResource("/ffmq").getPath());
            settings.setBooleanProperty(FFMQCoreSettings.SECURITY_ENABLED, true);
            settings.setStringProperty(FFMQCoreSettings.SECURITY_CONNECTOR, "net.timewalker.ffmq3.security.XMLSecurityConnector");
            settings.setStringProperty(FFMQServerSettings.SECURITY_CONNECTOR_XML_SECURITY, FFMQClientTest.class.getResource("/ffmq/conf/security.xml").getPath());
            return settings;
        }

        @Override
        public void startBroker() throws Exception {
            engine.deploy();
            tcpListener.start();
            if (!engine.getDestinationDefinitionProvider().hasQueueDefinition(QueueName)) {
                QueueDefinition queueDef = new QueueDefinition();
                queueDef.setName(QueueName);
                queueDef.setMaxNonPersistentMessages(10);
                engine.createQueue(queueDef);
            }
            if (!engine.getDestinationDefinitionProvider().hasTopicDefinition(TopicName)) {
                TopicDefinition topicDef = new TopicDefinition();
                topicDef.setName(TopicName);
                topicDef.setInitialBlockCount(10);
                topicDef.setAutoExtendAmount(10);
                topicDef.setMaxBlockCount(100);
                topicDef.setDataFolder(new File(FFMQClientTest.class.getResource("/ffmq").getPath()));
                engine.createTopic(topicDef);
            }
        }

        @Override
        public void stopBroker() throws Exception {
            tcpListener.stop();
            engine.undeploy();
        }

        @Override
        protected boolean populateJMSXUserIDSupported() {
            return false;
        }

        @Override
        public int getCurrentConnections() {
            try {
                Thread.sleep(10L);
            } catch (InterruptedException ex) {
            }
            return tcpListener.getActiveClients();
        }
    }

    public FFMQClientTest() {
        super(new FFMQBroker());
    }

    @Override
    public Client createClient(String username, String password) {
        return clientBuilder()
                .broker(MessageBroker.FFMQ)
                .converter(MessageConverter)
                .connection(BrokerURL)
                .user(username)
                .password(password)
                .build();
    }

}
