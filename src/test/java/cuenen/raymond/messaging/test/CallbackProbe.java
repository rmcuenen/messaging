package cuenen.raymond.messaging.test;

import cuenen.raymond.messaging.DataMessage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * CallbackFunction implementation used for testing.
 */
public class CallbackProbe implements Consumer<DataMessage> {

    private final Map<String, RuntimeException> errorMapping = new HashMap<>();
    private final List<DataMessage> received = new ArrayList<>();
    private final List<Throwable> errors = new ArrayList<>();

    @Override
    public void accept(DataMessage message) {
        if (message == null) {
            return;
        }
        received.add(message);
        final RuntimeException exception = errorMapping.get(message.getMessageType());
        if (exception != null) {
            throw exception;
        }
    }

    public void onError(Throwable cause) {
        if (cause != null) {
            errors.add(cause);
        }
    }

    public void setMessageFilter(DataMessage message, RuntimeException exception) {
        if (exception == null) {
            errorMapping.remove(message.getMessageType());
        } else {
            errorMapping.put(message.getMessageType(), exception);
        }
    }

    public List<DataMessage> getReceived() {
        return received;
    }

    public List<Throwable> getErrors() {
        return errors;
    }

    public int getReceiveCount() {
        return received.size();
    }

    public int getErrorCount() {
        return errors.size();
    }

    public void reset() {
        errors.clear();
        received.clear();
    }
}
