package cuenen.raymond.messaging.test;

import cuenen.raymond.messaging.Client;
import static cuenen.raymond.messaging.Client.MessageTypeHeader;
import cuenen.raymond.messaging.DataMessage;
import cuenen.raymond.messaging.MessageConverter;
import cuenen.raymond.messaging.MessageNotWriteableException;
import cuenen.raymond.messaging.MessageSelector;
import static cuenen.raymond.messaging.MessageSelector.MessageSelectorBuilder.*;
import cuenen.raymond.messaging.Request;
import cuenen.raymond.messaging.Subscriber;
import static cuenen.raymond.messaging.Subscriber.Default;
import cuenen.raymond.messaging.Timeout;
import static cuenen.raymond.messaging.test.Broker.*;
import cuenen.raymond.messaging.transport.JavaMessage;
import cuenen.raymond.messaging.transport.JavaSerializer;
import java.math.BigDecimal;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Abstract class for testing the implementation of a client.
 */
public abstract class ClientTest {

    protected static final String Username = "test-user";
    protected static final String Password = "secret";
    protected static final String SubscriberID = "test-subscriber";
    protected static final String Property = "test_property";
    protected static final MessageConverter MessageConverter = new JavaSerializer();
    protected static final Consumer<DataMessage> NoCallback = d -> {
    };

    protected final Broker broker;

    protected ClientTest(Broker broker) {
        this.broker = broker;
    }

    public abstract Client createClient(String username, String password);

    @Before
    public void setUp() throws Exception {
        broker.start();
    }

    @After
    public void tearDown() throws Exception {
        broker.stop();
    }

    /**
     * Test the connection to the message broker.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testConnection() throws Exception {
        assertEquals(createLogMessage("No connections should be present"), 0, broker.getCurrentConnections());
        final Client client = createClient();
        try {
            client.connect();
            assertEquals(createLogMessage("The client should be connected"), 1, broker.getCurrentConnections());
            try {
                client.connect();
            } catch (Exception ex) {
                fail(createLogMessage("Calling connect twice should not fail"));
            }
            assertEquals(createLogMessage("There should still be one client connected"), 1, broker.getCurrentConnections());
        } finally {
            client.close();
        }
        assertEquals(createLogMessage("The client should be disconnected"), 0, broker.getCurrentConnections());
        try {
            client.close();
        } catch (Exception ex) {
            fail(createLogMessage("Calling close twice should not fail"));
        }
        try {
            client.connect();
        } catch (Exception ex) {
            fail(createLogMessage("Re-connecting should not fail"));
        } finally {
            client.close();
        }
    }

    /**
     * Test the user credentials for connecting to the message broker.
     */
    @Test
    public void testCredentials() {
        try {
            final Client client = createClient("foo", "foo");
            client.connect();
            fail(createLogMessage("User 'foo' should not be permitted to connect"));
        } catch (Exception ex) {
            // Expected
        }
        try {
            final Client client = createClient(Username, "foo");
            client.connect();
            fail(createLogMessage("User should not be permitted to connect with invalid password"));
        } catch (Exception ex) {
            // Expected
        }
        try {
            final Client client = createClient(null, Password);
            client.connect();
            fail(createLogMessage("Default user should not be permitted to connect with custom password"));
        } catch (Exception ex) {
            // Expected
        }
        try {
            final Client client = createClient(null, null);
            client.connect();
            fail("Default user should not be permitted to connect with default password");
        } catch (Exception ex) {
            // Expected
        }
    }

    /**
     * Test the publication of messages.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testPublish() throws Exception {
        final DataMessage message = JavaMessage.wrap("Hello World");
        message.setBooleanProperty(Property, true);
        final CallbackProbe callback = new CallbackProbe();
        try (Client client = createClient()) {
            try {
                client.publish(TopicName, message);
                fail("Cannot publish to an un-connected client");
            } catch (Exception ex) {
                // Expected
            }
            client.connect();
            // Subscribe
            final Subscriber subscriber = Subscriber.create(TopicName, Default, callback);
            client.subscribe(subscriber);
            // Publish
            client.publish(TopicName, message);
            Thread.sleep(100L);
            assertEquals(createLogMessage("Should have received one message"), 1, callback.getReceiveCount());
            final DataMessage received = callback.getReceived().get(0);
            assertEquals(message, received);
            try {
                received.setBooleanProperty(Property, false);
                fail("Received messages should be read-only");
            } catch (MessageNotWriteableException ex) {
                // Expected
            }
            try {
                message.setBooleanProperty(Property, false);
                fail("Send messages should be read-only");
            } catch (MessageNotWriteableException ex) {
                // Expected
            }
        }
    }

    /**
     * Test the subscription management.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testSubscription() throws Exception {
        final DataMessage message = JavaMessage.wrap("Hello World");
        message.setStringProperty(Property, "some-value");
        final DataMessage failure = JavaMessage.wrap(800);
        final CallbackProbe callback = new CallbackProbe();
        callback.setMessageFilter(failure, new ArithmeticException());
        final Subscriber emptySubscriber = Subscriber.create(TopicName, SubscriberID, NoCallback);
        assertFalse("Subscriber should never be equal to any object", emptySubscriber.equals(new Object()));
        try (Client client = createClient()) {
            try {
                client.subscribe(emptySubscriber);
                fail(createLogMessage("Cannot call subscribe on a disconnected client"));
            } catch (IllegalStateException ex) {
                // Expected
            }
            client.connect();
            // Subscribe
            final Subscriber subscriber = Subscriber.create(TopicName, SubscriberID, callback);
            assertFalse("Subscribers should not be equal", subscriber.equals(emptySubscriber));
            assertNotEquals("Subscribers should not have the same hash code", subscriber.hashCode(), emptySubscriber.hashCode());
            client.subscribe(subscriber);
            broker.publish(message);
            assertEquals(createLogMessage("Should have received one message"), 1, callback.getReceiveCount());
            final DataMessage received = callback.getReceived().get(0);
            assertEquals(message, received);
            assertEquals("some-value", received.getStringProperty(Property));
            assertEquals(message.getMessageType(), received.getStringProperty(MessageTypeHeader));
            try {
                received.setStringProperty(Property, "some-other-value");
                fail("Received messages should be read-only");
            } catch (MessageNotWriteableException ex) {
                // Expected
            }
            callback.reset();
            // Subscribe different id
            final Subscriber other = Subscriber.create(subscriber.getTopicName(), callback);
            client.subscribe(other);
            broker.publish(failure);
            assertEquals(createLogMessage("Should have received the message twice"), 2, callback.getReceiveCount());
            assertTrue(callback.getReceived().stream().allMatch(failure::equals));
            callback.reset();
            // Unsubscribe the second id
            client.unsubscribe(other);
            message.setNumberProperty(Property + 2, 1200);
            broker.publish(message);
            assertEquals(createLogMessage("Should have received one message"), 1, callback.getReceiveCount());
            final DataMessage received2 = callback.getReceived().get(0);
            assertEquals(message, received2);
            assertEquals("some-value", received2.getStringProperty(Property));
            assertEquals(1200, received2.getNumberProperty(Property + 2));
            assertEquals(message.getMessageType(), received2.getStringProperty(MessageTypeHeader));
            callback.reset();
            // Subscribe other callback, same id
            client.subscribe(emptySubscriber);
            broker.publish(failure);
            assertEquals(createLogMessage("Should not have received any messages"), 0, callback.getReceiveCount());
        }
    }

    /**
     * Test the message selector.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testMessageSelector() throws Exception {
        final CallbackProbe callback = new CallbackProbe();
        final MessageSelector allSelector = allMessageSelector()
                .setIntProperty("weight", 2500)
                .setStringProperty("color", "blue")
                .build();
        final MessageSelector anySelector = anyMessageSelector()
                .setNullProperty("length")
                .setStringProperty("color", "gold")
                .setIntProperty("weight", 1500)
                .build();
        try (Client client = createClient()) {
            client.connect();
            client.subscribe(Subscriber.create(TopicName, SubscriberID, allSelector, callback));
            publishMessages();
            assertEquals(createLogMessage("Selector for weight=2500 AND color='blue'"), 2, callback.getReceiveCount());
            callback.reset();
            client.subscribe(Subscriber.create(TopicName, SubscriberID, anySelector, callback));
            publishMessages();
            assertEquals(createLogMessage("Selector for weight=1500 OR color='gold' OR length is present"), 2, callback.getReceiveCount());
            callback.reset();
            client.subscribe(Subscriber.create(TopicName, SubscriberID, callback));
            publishMessages();
            assertEquals(createLogMessage("Default selector should select everything"), 4, callback.getReceiveCount());
        }
    }

    /**
     * Test the request procedure.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRequest() throws Exception {
        final DataMessage message = JavaMessage.wrap("Hello World");
        final DataMessage reply = JavaMessage.wrap("Ok");
        final CallbackProbe callback = new CallbackProbe();
        final Timeout timeout = Timeout.create("1 second");
        try (Client client = createClient()) {
            try {
                client.send(Request.create(QueueName, message, timeout));
                fail(createLogMessage("Cannot call request on a disconnected client"));
            } catch (IllegalStateException ex) {
                // Expected
            }
            client.connect();
            client.send(Request.create(QueueName, message, timeout))
                    .whenComplete((result, error) -> {
                        callback.accept(result);
                        callback.onError(error);
                    });
            broker.expectMessage(message, reply);
            Thread.sleep(timeout.toMillis());
            try {
                message.setBooleanProperty(Property, Boolean.TRUE);
                fail("Send messages should be read-only");
            } catch (MessageNotWriteableException ex) {
                // Expected
            }
            assertEquals(createLogMessage("Should have received the reply"), 1, callback.getReceiveCount());
            final DataMessage received = callback.getReceived().get(0);
            assertEquals(reply, received);
            assertEquals(createLogMessage("Should not have received any errors"), 0, callback.getErrorCount());
            callback.reset();
            try {
                received.setNumberProperty(Property, BigDecimal.ONE);
                fail("Received messages should be read-only");
            } catch (MessageNotWriteableException ex) {
                // Expected
            }
            client.send(Request.create(QueueName, message, timeout))
                    .whenComplete((result, error) -> {
                        callback.accept(result);
                        callback.onError(error);
                    });
            broker.expectMessage(message, null);
            Thread.sleep(timeout.toMillis());
            assertEquals(createLogMessage("Should not have received the reply"), 0, callback.getReceiveCount());
            assertEquals(createLogMessage("Should have received a timeout error"), 1, callback.getErrorCount());
            assertTrue(callback.getErrors().get(0) instanceof TimeoutException);
        }
    }

    private void publishMessages() throws Exception {
        final DataMessage message1 = JavaMessage.wrap("Hello World");
        message1.setNumberProperty("weight", 2500);
        message1.setStringProperty("color", "blue");
        broker.publish(message1);
        final DataMessage message2 = JavaMessage.wrap("Hello World");
        message2.setNumberProperty("weight", 2500);
        message2.setStringProperty("color", "red");
        broker.publish(message2);
        final DataMessage message3 = JavaMessage.wrap("Hello World");
        message3.setNumberProperty("weight", 1500);
        message3.setStringProperty("color", "green");
        broker.publish(message3);
        final DataMessage message4 = JavaMessage.wrap("Hello World");
        message4.setNumberProperty("weight", 2500);
        message4.setStringProperty("color", "blue");
        message4.setNumberProperty("length", 6.5);
        message4.setBooleanProperty("available", false);
        broker.publish(message4);
    }

    private Client createClient() {
        return createClient(Username, Password);
    }

    private String createLogMessage(String message) {
        return getClass().getSimpleName() + " - " + message;
    }
}
