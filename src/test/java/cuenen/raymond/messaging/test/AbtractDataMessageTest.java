package cuenen.raymond.messaging.test;

import cuenen.raymond.messaging.AbstractDataMessage;
import cuenen.raymond.messaging.MessageFormatException;
import cuenen.raymond.messaging.MessageNotWriteableException;
import cuenen.raymond.messaging.MessagePropertyNotFoundException;
import java.math.BigInteger;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class AbtractDataMessageTest {

    private static final String PropertyToSet = "property-to-set";
    private static final String PropertyToOverride = "property-to-override";
    private static final String NullProperty = "null-property";
    private static final String MissingProperty = "missing-property";
    private static final String OtherTypedProperty = "other-typed-property";
    private AbstractDataMessage instance;

    @Before
    public void setUp() {
        instance = new AbstractDataMessage() {

            @Override
            public String getMessageType() {
                return "TestMessage";
            }

        };
    }

    /**
     * Test the handling of boolean properties.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBooleanProperty() throws Exception {
        // Retrieve set property
        instance.setBooleanProperty(PropertyToSet, Boolean.TRUE);
        assertTrue("Boolean property to set", instance.getBooleanProperty(PropertyToSet));
        // Overwrite property
        instance.setBooleanProperty(PropertyToOverride, Boolean.TRUE);
        instance.setBooleanProperty(PropertyToOverride, Boolean.FALSE);
        assertFalse("Boolean property to override", instance.getBooleanProperty(PropertyToOverride));
        // No property value
        instance.setBooleanProperty(NullProperty, null);
        assertNull(instance.getBooleanProperty(NullProperty));
        // Missing property
        try {
            instance.getBooleanProperty(MissingProperty);
            fail("Boolean prooperty missing");
        } catch (MessagePropertyNotFoundException ex) {
            // Expected
        }
        // Property of other type
        instance.setNumberProperty(OtherTypedProperty, 1234);
        try {
            instance.getBooleanProperty(OtherTypedProperty);
            fail("Boolean property of other type");
        } catch (MessageFormatException ex) {
            // Expected
        }
        // Convertable property
        instance.setStringProperty(OtherTypedProperty, "true");
        assertTrue("String property 'true' is convertable", instance.getBooleanProperty(OtherTypedProperty));
    }

    /**
     * Test the handling of number properties.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testNumberProperty() throws Exception {
        // Retrieve set property
        instance.setNumberProperty(PropertyToSet, BigInteger.TEN);
        assertEquals("Number property to set", BigInteger.TEN, instance.getNumberProperty(PropertyToSet));
        // Overwrite property
        instance.setNumberProperty(PropertyToOverride, -1);
        instance.setNumberProperty(PropertyToOverride, 0.0);
        assertEquals("Number property to override", 0.0, instance.getNumberProperty(PropertyToOverride));
        // No property value
        instance.setNumberProperty(NullProperty, null);
        assertNull(instance.getNumberProperty(NullProperty));
        // Missing property
        try {
            instance.getNumberProperty(MissingProperty);
            fail("Number prooperty missing");
        } catch (MessagePropertyNotFoundException ex) {
            // Expected
        }
        // Property of other type
        instance.setStringProperty(OtherTypedProperty, "six");
        try {
            instance.getNumberProperty(OtherTypedProperty);
            fail("Number property of other type");
        } catch (MessageFormatException ex) {
            // Expected
        }
        // Convertable property
        instance.setStringProperty(OtherTypedProperty, "32768");
        assertEquals("String property '32768' is convertable", 32768, instance.getNumberProperty(OtherTypedProperty));
        instance.setStringProperty(OtherTypedProperty, "3.14");
        assertEquals("String property '3.14' is convertable", 3.14f, instance.getNumberProperty(OtherTypedProperty));
    }

    /**
     * Test the handling of string properties.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testStringProperty() throws Exception {
        // Retrieve set property
        instance.setStringProperty(PropertyToSet, "Hello Messaging!");
        assertEquals("String property to set", "Hello Messaging!", instance.getStringProperty(PropertyToSet));
        // Overwrite property
        instance.setStringProperty(PropertyToOverride, "Hello Messaging!");
        instance.setStringProperty(PropertyToOverride, "Hello World!");
        assertEquals("String property to override", "Hello World!", instance.getStringProperty(PropertyToOverride));
        // No property value
        instance.setStringProperty(NullProperty, null);
        assertNull(instance.getStringProperty(NullProperty));
        // Missing property
        try {
            instance.getStringProperty(MissingProperty);
            fail("String prooperty missing");
        } catch (MessagePropertyNotFoundException ex) {
            // Expected
        }
        // Convertable property
        instance.setBooleanProperty(OtherTypedProperty, true);
        assertEquals("Any property is convertable", "true", instance.getStringProperty(OtherTypedProperty));
    }

    /**
     * Test the handling of Java object properties.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testObjectProperty() throws Exception {
        // Retrieve set property
        instance.setObjectProperty(PropertyToSet, BigInteger.ONE);
        assertEquals("Object property to set", BigInteger.ONE, instance.getObjectProperty(PropertyToSet));
        // Overwrite property
        instance.setObjectProperty(PropertyToOverride, Short.MAX_VALUE);
        instance.setObjectProperty(PropertyToOverride, Boolean.TRUE);
        assertEquals("Object property to override", Boolean.TRUE, instance.getObjectProperty(PropertyToOverride));
        // No property value
        instance.setObjectProperty(NullProperty, null);
        assertNull(instance.getObjectProperty(NullProperty));
        // Missing property
        try {
            instance.getObjectProperty(MissingProperty);
            fail("Object prooperty missing");
        } catch (MessagePropertyNotFoundException ex) {
            // Expected
        }
        // Property of other type
        try {
            instance.setObjectProperty(OtherTypedProperty, new Object());
            fail("Object property of other type");
        } catch (MessageFormatException ex) {
            // Expected
        }
    }

    /**
     * Test the existence of properties.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testPropertyExists() throws Exception {
        instance.setObjectProperty(PropertyToSet, Math.PI);
        instance.setObjectProperty(NullProperty, null);
        assertTrue(PropertyToSet + " should exist", instance.propertyExists(PropertyToSet));
        assertFalse(PropertyToOverride + " should not exist", instance.propertyExists(PropertyToOverride));
        assertTrue(NullProperty + " should exist", instance.propertyExists(NullProperty));
        assertFalse(MissingProperty + " should not exist", instance.propertyExists(MissingProperty));
        assertFalse(OtherTypedProperty + " should not exist", instance.propertyExists(OtherTypedProperty));
    }

    /**
     * Test the retrieval of all the property names.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetPropertyNames() throws Exception {
        instance.setObjectProperty(PropertyToSet, Math.PI);
        instance.setObjectProperty(NullProperty, null);
        Set<String> properties = new HashSet<>();
        properties.add(PropertyToSet);
        properties.add(NullProperty);
        for (Enumeration<String> names = instance.getPropertyNames(); names.hasMoreElements();) {
            String name = names.nextElement();
            if (properties.contains(name)) {
                properties.remove(name);
            } else {
                fail("Unexpected property: " + name);
            }
        }
        assertTrue("Missing property names", properties.isEmpty());
    }

    /**
     * Test read-only and clear properties.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testPropertyState() throws Exception {
        instance.setBooleanProperty(PropertyToSet, Boolean.TRUE);
        instance.setNumberProperty(NullProperty, null);
        instance.setObjectProperty(PropertyToOverride, Math.E);
        instance.onMarshall();
        try {
            instance.setObjectProperty(PropertyToOverride, Boolean.FALSE);
            fail("Cannot override read-only properties");
        } catch (MessageNotWriteableException ex) {
            // Expected
        }
        try {
            instance.setStringProperty(OtherTypedProperty, "new");
            fail("Cannot add read-only properties");
        } catch (MessageNotWriteableException ex) {
            // Expected
        }
        assertNull("Should be able to retrieve any null property", instance.getBooleanProperty(NullProperty));
        assertTrue("Original boolean property should still exist", instance.getBooleanProperty(PropertyToSet));
        instance.clearProperties();
        instance.setStringProperty(PropertyToOverride, "cleared");
        assertEquals("cleared", instance.getStringProperty(PropertyToOverride));
        try {
            instance.getBooleanProperty(PropertyToSet);
            fail("Original boolean property should be removed");
        } catch (MessagePropertyNotFoundException ex) {
            // Expected
        }
    }

    @Test
    public void testClassIdentifcation() throws Exception {
        instance.setBooleanProperty(PropertyToSet, Boolean.TRUE);
        instance.setNumberProperty(NullProperty, null);
        instance.setObjectProperty(PropertyToOverride, Math.E);
        assertFalse("Non data messages should never be equal", instance.equals(new Object()));
        assertTrue("Instance should be equal to itself", instance.equals(instance));
        final AtomicReference<String> type = new AtomicReference<>(instance.getMessageType());
        AbstractDataMessage other = new AbstractDataMessage() {

            @Override
            public String getMessageType() {
                return type.get();
            }
        };
        assertEquals("Wrong representation", instance.getMessageType() + "{}", other.toString());
        // Empty instance
        assertFalse("Empty instance should not be equal", instance.equals(other));
        assertNotEquals("Empty instance should have different hash codes", instance.hashCode(), other.hashCode());
        // Missing property
        other.setBooleanProperty(PropertyToSet, Boolean.TRUE);
        other.setNumberProperty(NullProperty, null);
        assertFalse("Instances with missing properties should not be equal", instance.equals(other));
        assertNotEquals("Instances with missing properties should have different hash codes", instance.hashCode(), other.hashCode());
        // Extra property
        other.setObjectProperty(PropertyToOverride, Math.E);
        other.setStringProperty(OtherTypedProperty, "Hello");
        assertFalse("Instances with extra properties should not be equal", instance.equals(other));
        assertNotEquals("Instances with extra properties should have different hash codes", instance.hashCode(), other.hashCode());
        // Different property value
        other.clearProperties();
        other.setStringProperty(PropertyToSet, "true");
        other.setNumberProperty(NullProperty, null);
        other.setObjectProperty(PropertyToOverride, Math.E);
        assertFalse("Instances with other property values should not be equal", instance.equals(other));
        assertNotEquals("Instances with other property values should have different hash codes", instance.hashCode(), other.hashCode());
        // Same instance
        other.setBooleanProperty(PropertyToSet, Boolean.TRUE);
        assertTrue("Instances with same properties should be equal", instance.equals(other));
        assertEquals("Instances with same properties should have the same hash codes", instance.hashCode(), other.hashCode());
        // Other state
        other.onMarshall();
        assertTrue("Instance state (read-only) does not effect equality", instance.equals(other));
        assertEquals("Instance state (read-only) does not effect hash code", instance.hashCode(), other.hashCode());
        // Other type
        type.set("Message");
        assertFalse("Instances of other message type should not be equal", instance.equals(other));
        assertEquals("Instances of other message type with the same properties should have the same hash codes", instance.hashCode(), other.hashCode());
    }
}
