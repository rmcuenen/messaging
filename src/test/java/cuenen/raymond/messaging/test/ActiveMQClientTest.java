package cuenen.raymond.messaging.test;

import cuenen.raymond.messaging.Client;
import static cuenen.raymond.messaging.Client.ClientBuilder.clientBuilder;
import cuenen.raymond.messaging.MessageBroker;
import static cuenen.raymond.messaging.test.JMSBroker.findFreePort;
import java.util.Arrays;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerPlugin;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.security.AuthenticationUser;
import org.apache.activemq.security.SimpleAuthenticationPlugin;

/**
 * Test the implementation of the ActiveMQ client.
 */
public class ActiveMQClientTest extends ClientTest {

    private static final String BrokerURL = "tcp://localhost:" + findFreePort(61616);

    private static class ActiveMQBroker extends JMSBroker {

        private final BrokerService broker = new BrokerService();

        public ActiveMQBroker() {
            super(new ActiveMQConnectionFactory("vm://localhost"));
            broker.setPersistent(false);
            broker.setPopulateJMSXUserID(true);
            final SimpleAuthenticationPlugin plugin = new SimpleAuthenticationPlugin();
            plugin.setUsers(Arrays.asList(
                    new AuthenticationUser(Username, Password, "user"),
                    new AuthenticationUser(Admin, Admin, "admin")));
            broker.setPlugins(new BrokerPlugin[]{plugin});
        }

        @Override
        public void startBroker() throws Exception {
            broker.addConnector(BrokerURL);
            broker.start();
        }

        @Override
        public void stopBroker() throws Exception {
            broker.stop();
        }

        @Override
        protected boolean populateJMSXUserIDSupported() {
            return broker.isPopulateJMSXUserID();
        }

        @Override
        public int getCurrentConnections() {
            return broker.getCurrentConnections() - 1;
        }
    }

    public ActiveMQClientTest() {
        super(new ActiveMQBroker());
    }

    @Override
    public Client createClient(String username, String password) {
        return clientBuilder()
                .broker(MessageBroker.ActiveMQ)
                .converter(MessageConverter)
                .connection(BrokerURL)
                .user(username)
                .password(password)
                .build();
    }
}
