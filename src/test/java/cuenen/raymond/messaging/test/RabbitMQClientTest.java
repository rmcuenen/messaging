package cuenen.raymond.messaging.test;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AMQP.Exchange.Declare;
import com.rabbitmq.client.AMQP.Queue.BindOk;
import com.rabbitmq.client.AMQP.Queue.DeclareOk;
import com.rabbitmq.client.AuthenticationFailureException;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.impl.LongStringHelper;
import cuenen.raymond.messaging.Client;
import static cuenen.raymond.messaging.Client.ClientBuilder.clientBuilder;
import cuenen.raymond.messaging.DataMessage;
import cuenen.raymond.messaging.MessageBroker;
import static cuenen.raymond.messaging.XMatch.*;
import cuenen.raymond.messaging.client.RabbitMQClient;
import static cuenen.raymond.messaging.test.ClientTest.MessageConverter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.*;

/**
 * Test the implementation of the RabbitMQ client.
 */
public class RabbitMQClientTest extends ClientTest {

    private static class ArgumentMap {

        private static final Object NULL = new Object();
        private final Map<String, Object> map = new ConcurrentHashMap<>();

        public void clear() {
            map.clear();
        }

        public Object get(String key) {
            final Object result = map.get(key);
            return NULL.equals(result) ? null : result;
        }

        public void putAll(Map<? extends String, ? extends Object> m) {
            m.entrySet().stream().forEach(entry -> map.put(entry.getKey(),
                    entry.getValue() == null ? NULL : entry.getValue()));
        }

        public Stream<Map.Entry<String, Object>> entryStream() {
            return map.entrySet().stream()
                    .map(e -> new Map.Entry<String, Object>() {

                        @Override
                        public String getKey() {
                            return e.getKey();
                        }

                        @Override
                        public Object getValue() {
                            return NULL.equals(e.getValue()) ? null : e.getValue();
                        }

                        @Override
                        public Object setValue(Object value) {
                            final Object result = map.put(e.getKey(), value == null ? NULL : value);
                            return NULL.equals(result) ? null : result;
                        }
                    });
        }

        private boolean containsKey(String key) {
            return map.containsKey(key);
        }
    }

    private static class RabbitMQBroker implements Broker {

        private final Channel channel = mock(Channel.class);
        private final AtomicInteger connections = new AtomicInteger(0);
        private final AtomicReference<QueueingConsumer.Delivery> delivery = new AtomicReference<>();
        private final List<String> consumerTags = new ArrayList<>();
        private final AtomicReference<Consumer> consumer = new AtomicReference<>();
        private final ArgumentMap arguments = new ArgumentMap();

        @Override
        public void start() throws Exception {
            when(Connection.createChannel()).then(m -> {
                connections.incrementAndGet();
                return channel;
            });
            doAnswer(m -> connections.decrementAndGet()).when(channel).close();
            final DeclareOk queue = mock(DeclareOk.class);
            when(channel.queueDeclare()).thenReturn(queue);
            when(queue.getQueue()).thenReturn("test-queue");
            when(channel.queueBind(eq("test-queue"), eq(TopicName), eq(""), anyMapOf(String.class, Object.class)))
                    .then(m -> {
                        arguments.clear();
                        arguments.putAll(m.getArgumentAt(3, Map.class));
                        return mock(BindOk.class);
                    });
            when(channel.basicConsume(eq("test-queue"), eq(true), anyString(), isA(Consumer.class)))
                    .then(m -> {
                        consumerTags.add(m.getArgumentAt(2, String.class));
                        consumer.set(m.getArgumentAt(3, Consumer.class));
                        return m.getArgumentAt(2, String.class);
                    });
            doAnswer(m -> {
                consumerTags.remove(m.getArgumentAt(0, String.class));
                return null;
            }).when(channel).basicCancel(anyString());
            doAnswer(m -> {
                final AMQP.BasicProperties properties = m.getArgumentAt(2, AMQP.BasicProperties.class);
                final byte[] body = m.getArgumentAt(3, byte[].class);
                final Envelope envelope = new Envelope(1L, false, "", "test-queue");
                delivery.set(new QueueingConsumer.Delivery(envelope, properties, body));
                return null;
            }).when(channel).basicPublish(eq(""), eq(QueueName), any(AMQP.BasicProperties.class), any(byte[].class));
            doAnswer(m -> {
                final AMQP.BasicProperties properties = m.getArgumentAt(2, AMQP.BasicProperties.class);
                final byte[] body = m.getArgumentAt(3, byte[].class);
                final Envelope envelope = new Envelope(1L, false, TopicName, "");
                final Consumer callback = consumer.get();
                for (String consumerTag : consumerTags) {
                    callback.handleDelivery(consumerTag, envelope, properties, body);
                }
                return null;
            }).when(channel).basicPublish(eq(TopicName), eq(""), any(AMQP.BasicProperties.class), any(byte[].class));
        }

        @Override
        public void stop() throws Exception {
            verify(ConnectionFactory, atLeastOnce()).newConnection();
        }

        @Override
        public int getCurrentConnections() {
            return connections.get();
        }

        @Override
        public void publish(DataMessage message) throws Exception {
            final Map<String, Object> headers = new HashMap<>();
            for (Enumeration<String> names = message.getPropertyNames(); names.hasMoreElements();) {
                final String name = names.nextElement();
                headers.put(name, message.getObjectProperty(name));
            }
            headers.put(Client.MessageTypeHeader, message.getMessageType());
            if (match(headers)) {
                headers.replaceAll((key, value) -> value instanceof String
                        ? LongStringHelper.asLongString((String) value) : value);
                final AMQP.BasicProperties basicProperties = new AMQP.BasicProperties.Builder()
                        .headers(headers).build();
                final Envelope envelope = new Envelope(1L, false, TopicName, "");
                final Consumer callback = consumer.get();
                for (String consumerTag : consumerTags) {
                    callback.handleDelivery(consumerTag, envelope, basicProperties, MessageConverter.encode(message));
                }
                Thread.sleep(10L);
            }
        }

        @Override
        public void expectMessage(DataMessage message, DataMessage reply) throws Exception {
            while (delivery.get() == null) {
                Thread.sleep(50);
            }
            final QueueingConsumer.Delivery deliver = delivery.get();
            assertEquals("Missing message type in header", message.getMessageType(), deliver.getProperties().getHeaders().get(Client.MessageTypeHeader));
            assertEquals("Missing username in header", Username, deliver.getProperties().getUserId());
            assertArrayEquals(MessageConverter.encode(message), deliver.getBody());
            if (reply != null) {
                final Map<String, Object> headers = new HashMap<>();
                headers.put(Client.MessageTypeHeader, reply.getMessageType());
                final AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
                        .correlationId(deliver.getProperties().getCorrelationId())
                        .headers(headers).build();
                consumer.get().handleDelivery("test-queue", deliver.getEnvelope(), properties, MessageConverter.encode(reply));
            }
            Thread.sleep(10L);
        }

        private boolean match(Map<String, Object> headers) {
            if (arguments == null) {
                return true;
            }
            final String type = String.valueOf(arguments.get(Argument));
            if (All.value().equals(type)) {
                return arguments.entryStream()
                        .filter(entry -> !entry.getKey().equals(Argument))
                        .allMatch(entry -> {
                            final Object messageValue = headers.get(entry.getKey());
                            return entry.getValue() != null && entry.getValue().equals(messageValue);
                        });
            } else if (Any.value().equals(type)) {
                return headers.entrySet().stream()
                        .filter(entry -> arguments.containsKey(entry.getKey()))
                        .anyMatch(entry -> {
                            final Object value = arguments.get(entry.getKey());
                            return value == null || entry.getValue().equals(value);
                        });
            } else {
                throw new IllegalArgumentException("Unknown matcher type: " + type);
            }
        }
    }
    private static final ConnectionFactory ConnectionFactory = mock(ConnectionFactory.class);
    private static final Declare ExchangeDeclare = mock(Declare.class);
    private static final Connection Connection = mock(Connection.class);

    @BeforeClass
    public static void setUpClass() throws Exception {
        doCallRealMethod().when(ConnectionFactory).setUsername(anyString());
        doCallRealMethod().when(ConnectionFactory).setPassword(anyString());
        when(ConnectionFactory.getUsername()).thenCallRealMethod();
        when(ConnectionFactory.getPassword()).thenCallRealMethod();
        doAnswer(m -> {
            if (Username.equals(ConnectionFactory.getUsername())
                    && Password.equals(ConnectionFactory.getPassword())) {
                return Connection;
            }
            throw new AuthenticationFailureException("Invalid username or password");
        }).when(ConnectionFactory).newConnection();
        when(ExchangeDeclare.getType()).thenReturn("headers");
        when(ExchangeDeclare.getDurable()).thenReturn(true);
        when(ExchangeDeclare.getAutoDelete()).thenReturn(false);
    }

    public RabbitMQClientTest() {
        super(new RabbitMQBroker());
    }

    @Override
    public Client createClient(String username, String password) {
        ConnectionFactory.setUsername(username);
        ConnectionFactory.setPassword(password);
        return new RabbitMQClient(MessageConverter, ConnectionFactory, ExchangeDeclare);
    }

    @Test
    public void testCreateRabbitMQClient() throws Exception {
        Client client = clientBuilder()
                .broker(MessageBroker.RabbitMQ)
                .converter(MessageConverter)
                .user(Username)
                .password(Password)
                .setStringProperty(RabbitMQClient.EXCHANGE_TYPE, "fanout")
                .build();
        assertTrue(client instanceof RabbitMQClient);
        // Extra test, so we have to invoke newConnection (see #stop).
        ConnectionFactory.setUsername(Username);
        ConnectionFactory.setPassword(Password);
        ConnectionFactory.newConnection();
    }
}
