package cuenen.raymond.messaging.test;

import cuenen.raymond.messaging.DataMessage;

/**
 * Wrapper / Stub of the message broker used during testing.
 */
public interface Broker {

    String TopicName = "BS_CAM";
    String QueueName = "MSG_SEND";

    void start() throws Exception;

    void stop() throws Exception;

    int getCurrentConnections();

    void publish(DataMessage message) throws Exception;

    void expectMessage(DataMessage message, DataMessage reply) throws Exception;

}
